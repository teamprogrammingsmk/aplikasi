<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Api'], function () {
    Route::group(['prefix' => 'admin'], function() {
        Route::name('admin.')->group(function() {
            Route::post('login', 'UserController@login');
            Route::post('pengajar', 'PengajarController@getPengajar');
        });
    });

    Route::group(['prefix' => 'siswa'], function() {
        Route::name('siswa.')->group(function() {
            Route::post('login', 'SiswaController@login');
        });
    });
    
    Route::group(['prefix' => 'mapel'], function() {
        Route::name('mapel.')->group(function() {
            Route::post('getMapel', 'MapelController@getMapel');
        });
    });
});