<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/author', function() {
    return view('author.index');
})->name('author');

Auth::routes(['register' => false, 'reset' => false, 'confirm' => false]);

Route::group(['namespace' => 'Admin',  'prefix' => 'admin', 'middleware' => ['auth', 'role:super-admin|admin']], function () {
    Route::name('admin.')->group(function () {
        // MASTER
        Route::resource('/kelas', 'KelasController', ['except' => ['show']])->middleware('permission:see-kelas|add-kelas|edit-kelas|delete-kelas');
        Route::resource('/siswa', 'SiswaController', ['except' => ['show']])->middleware('permission:see-siswa|add-siswa|edit-siswa|delete-siswa');
        
        // Guru
        Route::resource('/guru', 'UserController', ['except' => ['show']])->middleware('permission:see-guru|add-guru|edit-guru|delete-guru');
        Route::resource('/mapel', 'MapelController', ['except' => ['show']])->middleware('permission:see-mapel|add-mapel|edit-mapel|delete-mapel');
        Route::resource('/pengajar', 'PengajarController', ['except' => ['show']])->middleware('permission:see-pengajar|add-pengajar|edit-pengajar|delete-pengajar');
        Route::resource('/jurusan', 'JurusanController', ['except' => ['show']])->middleware('permission:see-jurusan|add-jurusan|edit-jurusan|delete-jurusan');
        Route::resource('/wali-kelas', 'WaliKelasController', ['except' => ['show', 'edit', 'update']]);
        Route::resource('/tahun-ajar', 'TahunAjarController', ['except' => ['show']])->middleware('permission:see-tahun-ajar|add-tahun-ajar|edit-tahun-ajar|delete-tahun-ajar');
        // Settings Route
        Route::group(['middleware' => ['role:super-admin|admin', 'permission:edit-settings']], function () {
            Route::get('/settings', 'SettingController@index')->name('settings.index');
            Route::put('/settings', 'SettingController@update')->name('settings.update');
        });
    });
});

Route::group(['middleware' => ['auth', 'role:super-admin|admin|user']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    // Jurnal
    Route::resource('/jurnal', 'JurnalController');
    Route::resource('/wali-kelas', 'WaliKelasController');
    Route::get('/all-jurnal', 'JurnalController@all')->name('jurnal.all');
    Route::get('/jurnal/detail/{id}', 'JurnalController@show_detail')->name('jurnal.detail');
    Route::post('/jurnal/siswa', 'JurnalController@siswa')->name('jurnal.siswa');
    Route::post('/jurnal/mapel', 'JurnalController@mapel')->name('jurnal.mapel');
    Route::get('/laporan/print-sdm', 'LaporanController@print')->name('coba-print');
    // Route::get('/laporan/all-coba-print', 'LaporanController@allprintsdm')->name('all-coba-print');
    Route::get('/laporan/sdm', 'LaporanController@indexsdm')->name('laporan_sdm.index')->middleware('permission:see-laporan-sdm');
    // Route::get('/laporan/all-sdm', 'LaporanController@indexallsdm')->name('laporan_sdm.all')->middleware('permission:see-laporan-sdm');
    Route::get('/laporan/bk', 'LaporanController@indexbk')->name('laporan_bk.index')->middleware('permission:see-laporan-bk');
    
    // Edit Profile
    Route::get('/profile', 'UserController@index')->name('user.index');
    Route::put('/profile', 'UserController@updateProfile')->name('user.updateProfile');

    // Edit Password
    Route::get('/edit-password', 'UserController@password')->name('user.password');
    Route::put('/edit-password', 'UserController@updatePassword')->name('user.updatePassword');
});
