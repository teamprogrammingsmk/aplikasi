@extends('layouts.auth')

@section('title', 'Author')

@section('content')
<div class="row">
    <a href="{{ route('login') }}" class="btn btn-warning text-white mb-3"><i class="fas fa-arrow-left"></i> Kembali</a>
    <div class="table-responsive">
        <h2>About Us</h2>
        <table class="table table-bordered table-striped mt-3">
            <thead>
                <tr>
                    <th>Author</th>
                </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <b>Name</b>: Alif Triadi Agung W<br />
                    <b>Sosial Link</b>: <ul>
                                    <li><a href="https://facebook.com/alftri.dev">Facebook</a></li>
                                </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Name</b>: Satria Yuda Pamungkas<br />
                    <b>Sosial Link</b>: <ul>
                                    <li><a href="https://facebook.com/satria.empty">Facebook</a></li>
                                </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Name</b>: Sugeng Budisaputra<br />
                    <b>Sosial Link</b>: <ul>
                                    <li><a href="https://facebook.com/putra.kivaara">Facebook</a></li>
                                </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Name</b>: Vanessa Florentina Patricia<br />
                    <b>Sosial Link</b>: <ul>
                                    <li><a href="https://facebook.com/vanessa.florentina.1">Facebook</a></li>
                                </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Name</b>: Josi Kie Nababan<br />
                    <b>Sosial Link</b>: <ul>
                                    <li><a href="https://facebook.com/kiejosi">Facebook</a></li>
                                </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Name</b>: M. Rohman Zainuri<br />
                    <b>Sosial Link</b>: <ul>
                                    <li><a href="https://facebook.com/rohman.roni.9">Facebook</a></li>
                                </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection