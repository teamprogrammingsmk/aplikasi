@extends('layouts.app')

@section('title', 'Home')

@section('content')
                    <div class="container-fluid page__container mt-4">
                        <div class="row">
                            <div class="col-lg">
                                <div class="card">
                                    <div class="card-body button-list">
                                        <div class="row d-flex justify-content-center">
                                            <img src="{{ asset('storage/images/logo/'. App\Models\Setting::setting()['web_logo']) }}" width="15%" alt="Logo">
                                        </div>
                                        <div class="row d-flex justify-content-center">
                                            <h1 class="welcome" data-webname="{{ App\Models\Setting::setting()['web_alias'] }}"></h1>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <p class="mb-0">Made with <i class="fas fa-heart text-danger"></i> by RPL 2018 <a class="btn btn-info btn-sm mt-0 float-right" href="{{ route('author') }}"><i class="fas fa-info"></i></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
@endsection

@section('script')
<script type="text/javascript">
    let webname = $('.welcome').data('webname');
    let typed = new Typed('.welcome', {
        strings: ["Selamat Datang di " + webname],
        typeSpeed: 40,
        delaySpeed : 90,
        loop: true
    });
</script>
@endsection