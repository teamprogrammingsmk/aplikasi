@extends('layouts.app')

@section('title','Detail Jurnal')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label for="jurnal_id" class="col-sm-2 col-form-label">Jurnal ID</label>
                                                <div class="col-sm-10">
                                                    <input id="jurnal_id" type="text" class="form-control @error('jurnal_id') is-invalid @enderror" value="{{ $jurnal->no_jurnal }}" disabled readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tanggal" class="col-sm-2 col-form-label">Tanggal</label>
                                                <div class="col-sm-10">
                                                    <input id="tanggal" type="date" class="form-control" data-toggle="flatpickr" placeholder="tanggal" value="{{ $jurnal->tanggal }}" readonly disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="guru" class="col-sm-2 col-form-label">Guru</label>
                                                <div class="col-sm-10">
                                                    <input id="guru" type="text" class="form-control" value="{{ $jurnal->user->name }}" readonly disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="mapel" class="col-sm-2 col-form-label">Mapel</label>
                                                <div class="col-sm-10">
                                                    <input id="mapel" type="text" class="form-control" value="{{ $jurnal->mapel->nama_mapel }}" readonly disabled>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="materi" class="col-sm-2 col-form-label">Materi</label>
                                                <div class="col-sm-10">
                                                    <textarea id="materi" type="text" class="form-control" readonly disabled>{{ $jurnal->materi }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">Absen</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th rowspan="2">NO</th>
                                                    <th rowspan="2">NAMA</th>
                                                    <th class="text-center" colspan="4">ABSEN</th>
                                                    <th class="text-center" rowspan="2">MORE</th>
                                                    <tr>
                                                        <th>H</th>
                                                        <th>S</th>
                                                        <th>I</th>
                                                        <th>A</th>
                                                    </tr>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($absen as $a)    
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $a->siswa->nama_siswa }}</td>
                                                        <td><div class="fas fa-{{ $a->presensi == 'hadir' ? 'check' : 'times'}} text-{{ $a->presensi == 'hadir' ? 'success' : 'danger'}}"></div></td>
                                                        <td><div class="fas fa-{{ $a->presensi == 'sakit' ? 'check' : 'times'}} text-{{ $a->presensi == 'sakit' ? 'success' : 'danger'}}"></div></td>
                                                        <td><div class="fas fa-{{ $a->presensi == 'ijin' ? 'check' : 'times'}} text-{{ $a->presensi == 'ijin' ? 'success' : 'danger'}}"></div></td>
                                                        <td><div class="fas fa-{{ $a->presensi == 'alpha' ? 'check' : 'times'}} text-{{ $a->presensi == 'alpha' ? 'success' : 'danger'}}"></div></td>
                                                        <td class="text-center"><a href="{{ route('jurnal.detail', $a->id) }}"> Read more</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
    
@endsection