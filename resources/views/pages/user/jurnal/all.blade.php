@extends('layouts.app')

@section('title', 'All Jurnal')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="table" class="table table-striped thead-border-top-0">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Materi</th>
                                                    <th>Mapel</th>
                                                    <th>Kelas</th>
                                                    <th>Tanggal</th>
                                                    <th style="width: 200px">Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "{{ route('jurnal.all') }}"
            },
            responsive: true,
            "columns": [
                {
                    data: 'no_jurnal',
                    name: 'no_jurnal'
                },
                {
                    data: 'materi',
                    name: 'materi'
                },
                {
                    data: 'mapel.nama_mapel',
                    name: 'mapel.nama_mapel'
                },
                {
                    data: 'kelas.nama_kelas',
                    name: 'kelas.nama_kelas'
                },
                {
                    data: 'tanggal',
                    name: 'tanggal'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        })
    })
</script>
@endsection