@extends('layouts.app')

@section('title', 'Edit Jurnal')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')
    <div class="container-fluid page__container mt-4">
        <div class="row">
            <div class="col-lg">
                <form action="{{ route('jurnal.update', $jurnal->id) }}" method="post" enctype="multipart/form-data">
                @method('put')
                @csrf
                <div class="card">
                    <div class="card-header card-header-large bg-white">
                        <div class="row d-flex">
                            <div class="col-md-6">
                                <h5 class="m-0">@yield('title')</h5>
                            </div>
                            <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="jurnal_id" class="col-sm-2 col-form-label">Jurnal ID</label>
                                            <div class="col-sm-10">
                                                <input id="jurnal_id" type="text" class="form-control @error('jurnal_id') is-invalid @enderror" value="{{ $jurnal->no_jurnal }}" disabled readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="tanggal" class="col-sm-2 col-form-label">Tanggal</label>
                                            <div class="col-sm-10">
                                                <input name="tanggal" id="tanggal" type="date" class="form-control @error('tanggal') is-invalid @enderror" data-toggle="flatpickr" value="{{ $jurnal->tanggal }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="kelas_id" class="col-sm-2 col-form-label">Kelas</label>
                                            <div class="col-sm-10">
                                                <input type="text" id="kelas_id" class="form-control" value="{{ $jurnal->kelas->nama_kelas }}" readonly disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <div class="form-group row">
                                            <label for="mapel_id" class="col-sm-2 col-form-label">Mapel</label>
                                            <div class="col-sm-10">
                                                <input type="text" id="mapel_id" class="form-control" value="{{ $jurnal->mapel->nama_mapel }}" readonly disabled>
                                            </div>
                                            @error('mapel_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="materi" class="col-sm-2 col-form-label">Materi</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control @error('materi') is-invalid @enderror" name="materi" id="materi" placeholder="Text here">{{ $jurnal->materi }}</textarea>
                                            </div>
                                            @error('materi')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header card-header-large bg-white">
                        <div class="row d-flex">
                            <div class="col-md-6">
                                <h5 class="m-0">Absen</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped siswa">
                                <thead>
                                    <tr>
                                        <th rowspan="2">NO</th>
                                        <th rowspan="2">NAMA</th>
                                        <th class="text-center" colspan="4">ABSEN</th>
                                        <th rowspan="2">MORE</th>
                                        <tr>
                                            <th>H</th>
                                            <th>S</th>
                                            <th>I</th>
                                            <th>A</th>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($absen as $a)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td><input type="hidden" name="siswaid[]" value="{{ $a->siswa->id }}"> {{ $a->siswa->nis }}</td>
                                            <td>{{ $a->siswa->nama_siswa }}</td>
                                            <td><input type='radio' name='absen_{{ $a->siswa->id }}' value='hadir' @if($a->presensi == 'hadir') checked @endif></td>
                                            <td><input type='radio' name='absen_{{ $a->siswa->id }}' value='sakit' @if($a->presensi == 'sakit') checked @endif></td>
                                            <td><input type='radio' name='absen_{{ $a->siswa->id }}' value='ijin' @if($a->presensi == 'ijin') checked @endif></td>
                                            <td><input type='radio' name='absen_{{ $a->siswa->id }}' value='alpha' @if($a->presensi == 'alpha') checked @endif></td>
                                            <td><textarea name="keterangan_{{ $a->siswa->id }}" class="form-control mb-3 input-jurnal-keterangan" placeholder="Keterangan">{{ $a->keterangan }}</textarea><input class='form-control input-jurnal-gambar' type='file' name='images_{{ $a->siswa->id }}'></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary float-right">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection