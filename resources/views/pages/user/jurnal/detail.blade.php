@extends('layouts.app')

@section('title', 'Detail jurnal')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <h5 class="m-0">Data Siswa</h5>
                                </div>
                                <div class="card-body">
                                    <p class="text-uppercase">
                                        <b>Nama</b> : {{ $absen->siswa->nama_siswa }}<br />
                                        <b>Kelas</b> : {{ $absen->jurnal->kelas->nama_kelas }}<br />
                                        <b>Presensi</b> : <b class="text-{{ $absen->presensi == 'alpha' ? 'danger' : 'success' }}">{{ $absen->presensi }}</b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <h5 class="m-0">Data Jurnal</h5>
                                </div>
                                <div class="card-body">
                                    <p class="text-uppercase">
                                        <b>No Jurnal</b> : <a href="{{ route('jurnal.show', $absen->jurnal->id) }}">#{{ $absen->jurnal->no_jurnal }}</a><br />
                                        <b>Tanggal</b> : {{ \Carbon\Carbon::parse($absen->jurnal->tanggal)->translatedFormat("l, d F Y") }}<br />
                                        <b>Materi</b> : {{ $absen->jurnal->materi }}<br />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <h5 class="m-0">Keterangan Lanjutan</h5>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p><b>Keterangan</b> : {{ is_null($absen->keterangan) ? 'Tidak ada keterangan' : $absen->keterangan }}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><b>Gambar</b> : @if(!$absen->image) Tidak ada gambar @else <br><br><img src='{{ asset('storage/images/absensi/' . $absen->image) }}' alt='gambar' class="rounded card-img-top"> @endif</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection