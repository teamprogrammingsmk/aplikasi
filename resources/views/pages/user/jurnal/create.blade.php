@extends('layouts.app')

@section('title', 'Tambah Jurnal')

@section('content')
    <div class="container-fluid page__container mt-4">
        <div class="row">
            <div class="col-lg">
                <form action="{{ route('jurnal.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header card-header-large bg-white">
                        <div class="row d-flex">
                            <div class="col-md-6">
                                <h5 class="m-0">@yield('title')</h5>
                            </div>
                            <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="jurnal_id" class="col-sm-2 col-form-label">Jurnal ID</label>
                                            <div class="col-sm-10">
                                                <input id="jurnal_id" type="text" class="form-control @error('jurnal_id') is-invalid @enderror" value="{{ $jurnal }}" disabled readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="tanggal" class="col-sm-2 col-form-label">Tanggal</label>
                                            <div class="col-sm-10">
                                                <input name="tanggal" id="tanggal" type="date" class="form-control @error('tanggal') is-invalid @enderror" data-toggle="flatpickr" value="today">
                                            </div>
                                            @error('tanggal')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="kelas_id" class="col-sm-2 col-form-label">Kelas</label>
                                            <div class="col-sm-10">
                                                <select name="kelas_id" id="kelas_id" data-toggle="select" class="form-control @error('kelas_id') is-invalid @enderror">
                                                    <option value="">Pilih kelas</option>
                                                    @foreach ($pengajar as $k)
                                                        <option value="{{ $k->kelas->id }}" @if(old('kelas_id') == $k->kelas->id) selected @endif>{{ $k->kelas->nama_kelas }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @error('kelas_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <div class="form-group row">
                                            <label for="mapel_id" class="col-sm-2 col-form-label">Mapel</label>
                                            <div class="col-sm-10">
                                                <select name="mapel_id" id="mapel_id" data-toggle="select" class="form-control @error('mapel_id') is-invalid @enderror">
                                                    <option value="">Pilih mapel</option>
                                                </select>
                                            </div>
                                            @error('mapel_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label for="materi" class="col-sm-2 col-form-label">Materi</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control @error('materi') is-invalid @enderror" name="materi" id="materi" placeholder="Text here">{{ old('materi') }}</textarea>
                                            </div>
                                            @error('materi')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header card-header-large bg-white">
                        <div class="row d-flex">
                            <div class="col-md-6">
                                <h5 class="m-0">Absen</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped siswa">
                                <thead>
                                    <tr>
                                        <th rowspan="2">NO</th>
                                        <th rowspan="2">NAMA</th>
                                        <th class="text-center" colspan="4">ABSEN</th>
                                        <th rowspan="2" colspan="2">MORE</th>
                                        <tr>
                                            <th>H</th>
                                            <th>S</th>
                                            <th>I</th>
                                            <th>A</th>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary float-right">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    $('#kelas_id').change(function() {
        $(".siswa tbody").html('');
        $("#mapel_id").html('');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            'url': "{{ route('jurnal.siswa') }}",
            'type': 'POST',
            data: {_token: CSRF_TOKEN, kelas_id:$(this).val()},
            success: function (data) { 
                $(".siswa tbody").append(data);
            }
        });

        $.ajax({
            'url': "{{ route('jurnal.mapel') }}",
            'type': 'POST',
            data: {_token: CSRF_TOKEN, kelas_id:$(this).val()},
            success: function (response) {
                let mapel = response.toString()

                $('#mapel_id').append(mapel)
            }
        });
    });
</script>
@endsection