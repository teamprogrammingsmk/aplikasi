@extends('layouts.app')

@section('title', 'Jurnal')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        @can('add-jurnal')
                                        <a href="{{ route('jurnal.create') }}" class="btn btn-primary ml-auto mr-3"><i class="fas fa-plus-square"></i></a>
                                        @endcan
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="table" class="table table-striped thead-border-top-0">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Materi</th>
                                                    <th>Mapel</th>
                                                    <th>Kelas</th>
                                                    <th>Tanggal</th>
                                                    <th style="width: 200px">Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "{{ route('jurnal.index') }}"
            },
            responsive: true,
            "columns": [
                {
                    data: 'no_jurnal',
                    name: 'no_jurnal'
                },
                {
                    data: 'materi',
                    name: 'materi'
                },
                {
                    data: 'mapel.nama_mapel',
                    name: 'mapel.nama_mapel'
                },
                {
                    data: 'kelas.nama_kelas',
                    name: 'kelas.nama_kelas'
                },
                {
                    data: 'tanggal',
                    name: 'tanggal'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        })

        $('#table').on('click', '.btn-danger[data-remote]', function (e) { 
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var url = $(this).data('remote');
            // confirm then
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data yang dihapus tidak bisa dikembalikan lagi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya!',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        type: 'DELETE',
                        dataType: 'json',
                        data: {method: '_DELETE', submit: true}
                    }).always(function (data) {
                        Swal.fire(
                        'Terhapus!',
                        'Data berhasil dihapus.',
                        'success'
                        )
                        $('#table').DataTable().draw(false);
                    });
                }
            })
        });
    })
</script>
@endsection