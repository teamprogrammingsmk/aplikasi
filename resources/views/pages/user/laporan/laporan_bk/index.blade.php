@extends('layouts.app')

@section('title', 'Laporan BK')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                <form action="{{url()->current()}}" method="get">

                                  {{-- BAGIAN PILIHAN REKAPAN --}}
                                  <div class="form-group row">
                                    <label for="pilihrekap" class="col-sm-2 col-form-label">Pilihan Rekapan</label>
                                    <div class="col-sm-10">
                                      <select  data-toggle="select" name="pilihrekap" id="pilihrekap" class="form-control" required>
                                        <option value="" disabled selected hidden>Pilih Rekap</option>
                                        <option value="1">Rentang waktu</option>
                                        <option value="2">Bulanan</option>
                                        <option value="3">Semester</option>
                                      </select>
                                    </div>
                                  </div>
                                  {{-- BAGIAN KELAS --}}
                                    <div class="form-group row">
                                      <label for="bulan" class="col-sm-2 col-form-label">Kelas</label>
                                        <div class="col-sm-10">
                                            <select  data-toggle="select" name="kelas" id="kelas" class="form-control" required>
                                                <option value="" disabled selected hidden>Pilih Kelas</option>
                                                    @foreach ($kelas as $k)
                                                <option value="{{$k->id}}">{{$k->nama_kelas}}</option>
                                                    @endforeach

                                            </select>
                                        </div>
                                      </div>
                                                {{-- BAGIAN TANGGAL DARI SAMPAI TANGGAL --}}
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="tanggaldari" class="col-sm-4 col-form-label">Rentang</label>
                                                            <div class="col-sm-8">
                                                              <input type="date"  class="form-control" id="tanggaldari" name="tanggaldari" data-toggle="flatpickr" value="today" placeholder="Dari Tanggal" required disabled >
                                                            </div>
                                                          </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="tanggalsampai" class="col-sm-1 col-form-label">-</label>
                                                            <div class="col-sm-11">
                                                              <input type="date"  class="form-control" id="tanggalsampai" name="tanggalsampai" data-toggle="flatpickr" value="today" placeholder="Sampai Tanggal" required disabled>
                                                            </div>
                                                          </div>
                                                    </div>
                                                </div>
                                                
                                                  {{-- BAGIAN BULAN --}}
                                                  <div class="form-group row">
                                                    <label for="bulan" class="col-sm-2 col-form-label">Bulan</label>
                                                    <div class="col-sm-10">
                                                      <select  data-toggle="select" name="Bulan" id="bulan" class="form-control" required disabled>
                                                        <option value="" disabled selected hidden>Pilih Bulan</option>
                                                        
                                                          @foreach ($bulan as $b)
                                                          <option value="{{$b['no']}}">{{$b['bulan']}}</option>
                                                          @endforeach
                                                        
                                                      </select>
                                                    </div>
                                                  </div>
                                                  {{-- BAGIAN SEMESTER --}}
                                                  <div class="form-group row">
                                                    <label for="bulan" class="col-sm-2 col-form-label">Semester</label>
                                                    <div class="col-sm-10">
                                                      <select  data-toggle="select" name="Semester" id="semester" class="form-control" required disabled>
                                                        <option value="" disabled selected hidden>Pilih Semester</option>
                                                        
                                                          <option value="1">Ganjil</option>
                                                          <option value="2">Genap</option>
                                                        
                                                      </select>
                                                    </div>
                                                  </div>
                                                <button class="btn btn-primary float-right">Submit</button>
                                            </form>
                                                
                                    <div class="table-responsive">
                                        <table class="table table-striped thead-border-top-0">
                                            <thead>
                                                <tr>
                                                  <th rowspan="2">NO</th>
                                                  <th rowspan="2">NAMA</th>
                                                  <th class="text-center" colspan="4">ABSEN</th>
                                                  {{-- <th class="text-center" rowspan="2">MORE</th> --}}
                                                  <tr>
                                                      <th>H</th>
                                                      <th>S</th>
                                                      <th>I</th>
                                                      <th>A</th>
                                                  </tr>
                                              </tr>
                                            
                                            </thead>
                                            <tbody>
                                                    
                                                        @foreach ($absen as $d)
                                                    <tr>
                                                         <td>{{$loop->iteration}}</td>
                                                        <td>{{$d->siswa->nama_siswa}}</td>
                                                        {{-- <td>{{$d['absen']['hadir']}}</td>
                                                        <td>{{$d['absen']['sakit']}}</td>
                                                        <td>{{$d['absen']['ijin']}}</td>
                                                        <td>{{$d['absen']['alpha']}}</td> --}}
                                                      
                                                      
                                                    </tr>
                                                    @endforeach
                                                    

                                            </tbody>
                                        </table>
                                        <a href="{{ route('coba-print') }}" class="btn btn-info">Print</a>
                                    </div>
                                </div>
                                {{-- {{$dastabk->withQueryString()->links()}} --}}
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
<script type="text/javascript">$("#pilihrekap").change(function () {
  const bulan = $('#bulan')
  const tanggaldari = $('#tanggaldari')
  const tanggalsampai = $('#tanggalsampai')
  const semester = $('#semester')
  if ($("#pilihrekap").val() == 1) {
    tanggaldari.removeAttr('disabled');
    tanggalsampai.removeAttr('disabled');
    bulan.attr('disabled', 'disabled').css('background-color', 'grey');
    semester.attr('disabled', 'disabled').css('background-color', 'grey');
    
    } else if($("#pilihrekap").val() == 2){
    bulan.removeAttr('disabled');
    tanggaldari.attr('disabled', 'disabled');
    tanggalsampai.attr('disabled', 'disabled');
    semester.attr('disabled', 'disabled').css('background-color', 'red');
    
    } else if($("#pilihrekap").val() == 3){
    semester.removeAttr('disabled');
    tanggaldari.attr('disabled', 'disabled');
    tanggalsampai.attr('disabled', 'disabled');
    bulan.attr('disabled', 'disabled').css('background-color', 'grey');
    } else {
    }
}).trigger("change");</script>
@endsection