<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title') Laporan SDM {{$tanggalsampai}}</title>
    <style>
        div.head {
            margin-top: 6vh;
            margin-bottom: 5vh;
            width:400px;
            float:left;
            margin-left: 90px;
        }

        div.print {
            margin: auto;
            width: 75%;
        }

        #tabel_print {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #tabel_print thead tr th, #tabel_print tbody tr td{
            border: 1px solid #ddd;
            padding: 8px;
        }

        #tabel_print tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #tabel_print tr:hover {
            background-color: #ddd;
        }

        #tabel_print thead tr th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #d6d6d6;
            color: black;
        }
        .page-break {
    page-break-after: always ;
}
    </style>
</head>
<body onload="window.print()">
    @section('title','Print Laporan')
    
    @include('components.kop')
    <div>
        <div class="head">
            <table>
                <thead>
                    <tr>
                        <td align="left">Nama Instansi
                        <td>:
                        <td align="left">{{ App\Models\Setting::setting()['web_title'] }}
                    </tr>
                    <tr>
                        <td align="left">Tahun Ajaran
                        <td>:
                        <td align="left">{{ $pilihantahunajar }}
                    </tr>
                    <tr>
                        <td align="left">Periode Laporan
                        <td>:
                        <td align="left"> {{$tanggaldaricustom}} s.d. {{$tanggalsampaicustom}}
                    </tr>
                    <tr>
                        <td align="left">Nama Guru
                        <td>:
                        <td alig n="left">{{ $data[1]['name'] }}
                    </tr>
                </thead>
            </table>
        </div>
    </div> 
    <div class="table-responsive">
        <div class="print">
            <table id="tabel_print">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Mapel</th>
                        <th>Kelas</th>
                        <th>Materi</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=0 ?>
                    @foreach ($data as $d)  
                    <tr>
                        <?php $i++ ?>                  
                        <td><font size='2'>{{ $loop->iteration }}</font></td>
                        <td><font size='2'>{{ $d['tanggal'] }}</font></td>
                        <td><font size='2'>{{ $d['mapel'] }}</font></td>
                        <td><font size='2'>{{ $d['kelas'] }}</font></td>
                        <td><font size='2'>{{ $d['materi'] }}</font></td>
                        <td><font size='2'>H : {{ $d['absen']['hadir'] }}<br/> S : {{ $d['absen']['sakit'] }}<br/> I : {{ $d['absen']['ijin'] }}<br/> A : {{ $d['absen']['alpha'] }}</font></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
                    <?php
                        if( $i == 5 ){ 
                        echo '<div class="page-break"></div>';
                    }?>
        </div>
    </div>
    
    @include('components.ttd')

</body>
<script>
    window.onafterprint = function(){
        history.go(-1);
     }
</script>
</html>
