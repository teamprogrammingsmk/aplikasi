@extends('layouts.app')

@section('title', 'Laporan SDM')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                <form action="{{url()->current()}}" method="get">

                                                {{-- BAGIAN DARI TANGGAL KE TANGGAl --}}
                                                <div class="form-group row">
                                                  <label for="tahun_ajar" class="col-sm-2 col-form-label">Tahun Ajaran</label>
                                                  <div class="col-sm-10">
                                                    <input type="text" id="tahun_ajar" name="periode" class="form-control" value="2020 / 2021" readonly >
                                                  </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="tanggaldari" class="col-sm-4 col-form-label">Periode</label>
                                                            <div class="col-sm-8">
                                                              <input type="date"  class="form-control" id="tanggaldari" name="tanggaldari" data-toggle="flatpickr" placeholder="Dari Tanggal" required>
                                                            </div>
                                                          </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <label for="tanggalsampai" class="col-sm-2 col-form-label float-right">s.d.</label>
                                                            <div class="col-sm-10">
                                                              <input type="date"  class="form-control" id="tanggalsampai" name="tanggalsampai" data-toggle="flatpickr" placeholder="Sampai Tanggal" required>
                                                            </div>
                                                          </div>
                                                    </div>
                                                </div>
                                                {{-- BAGIAN KELAS --}}
                                                  {{-- BAGIAN BULAN --}}
                                                  <div class="form-group row">
                                                    <label for="bulan" class="col-sm-2 col-form-label">Guru</label>
                                                    <div class="col-sm-10">
                                                      <select  data-toggle="select" name="guru" id="guru" class="form-control" required>
                                                        <option value="" disabled selected hidden>Pilih Guru</option>
                                                        {{-- <option value="999">Semua Guru</option> --}}
                                                        @foreach ($user as $u)
                                                      <option value="{{$u->id}}">{{$u->name}}</option>
                                                        @endforeach
        
                                                      </select>
                                                    </div>
                                                  </div>
                                                  {{-- BAGIAN MAPEL --}}
                                                <button class="btn btn-primary float-right">Submit</button>
                                            </form>
                                                
                                    <div class="table-responsive">
                                        <table class="table table-striped thead-border-top-0">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal</th>
                                                    <th>Mapel</th>
                                                    <th>Kelas</th>
                                                    <th>Materi</th>
                                                    <th>Keterangan</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    
                                                        @foreach ($data as $d)
                                                    <tr>
                                                         <td>{{$loop->iteration}}</td>
                                                        <td>{{$d['tanggal']}}</td>
                                                        <td>{{$d['mapel']}}</td>
                                                        <td>{{$d['kelas']}}</td>
                                                        <td>{{$d['materi']}}</td>
                                                        <td>H = {{$d['absen']['hadir']}}<br>S = {{$d['absen']['sakit']}}<br>I = {{$d['absen']['ijin']}}<br>A = {{$d['absen']['alpha']}}<br></td>
                                                      
                                                    </tr>
                                                    @endforeach
                                                    

                                            </tbody>
                                        </table>
                                        <?php
                                        $periode = isset($_GET['periode']) ? $_GET['periode'] : false;
                                        $guru = isset($_GET['guru']) ? $_GET['guru'] : false;
                                        $tanggaldari = isset($_GET['tanggaldari']) ? $_GET['tanggaldari'] : false;
                                        $tanggalsampai = isset($_GET['tanggalsampai']) ? $_GET['tanggalsampai'] : false;
                                        $datas=$periode.$guru.$tanggaldari.$tanggalsampai;
                                        $query=is_null($datas)?null:"periode=$periode&tanggaldari=$tanggaldari&tanggalsampai=$tanggalsampai&guru=$guru";
                                        ?>
                                        <a href="{{ route('coba-print',$query) }}" class="btn btn-info">Print</a>
                                    </div>
                                  </div>
                                {{$dasta->withQueryString()->links()}}
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')

@endsection