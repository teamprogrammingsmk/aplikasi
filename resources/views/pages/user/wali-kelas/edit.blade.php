@extends('layouts.app')

@section('title', 'Aksi Wali Kelas')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{ url()->previous() }}" class="btn btn-danger float-right mr-3">Kembali</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-8">
                                            <form action="{{ route('wali-kelas.update', $siswa->id) }}" method="post" enctype="multipart/form-data">
                                                @method('put')
                                                @csrf
                                                <div class="form-group">
                                                    <label for="keterangan" class="form-label">Komentar</label>
                                                    <textarea name="keterangan" id="keterangan" class="form-control" placeholder="Komentar anda terhadap anak ini?">{{ $siswa->keterangan }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <label class="form-label">Preview</label><br />
                                                            <img src="{{ asset('storage/images/siswa/'. $siswa->image) }}" alt="Gambar siswa" style="width: 250px;height: 250px;object-fit: cover">
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <label for="image" class="form-label">Image</label>
                                                            <input type="file" name="image" id="image" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary float-right">Submit</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection