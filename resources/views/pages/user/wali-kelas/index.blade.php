@extends('layouts.app')

@section('title', 'Wali Kelas')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                            <p>Name: <strong>{{ $waliKelas->kelas->nama_kelas }}</strong></p>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{ url()->previous() }}" class="btn btn-danger float-right mr-3">Kembali</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="table-responsive">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>NAMA</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($kelas as $kls)
                                                        @foreach ($kls->siswa as $siswa)
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>{{ $siswa->nama_siswa }}</td>
                                                                <td>
                                                                    <a href="{{ route('wali-kelas.edit', $siswa->id) }}" class="btn btn-warning text-white"><i class="far fa-edit"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "{{ route('wali-kelas.index') }}",
            },
            responsive: true,
            "columns": [
                {
                    data: 'nis',
                    name: 'nis'
                },
                {
                    data: 'nama_siswa',
                    name: 'nama_siswa'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        })
    })
</script>
@endsection