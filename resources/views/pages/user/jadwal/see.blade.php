@extends('layouts.app')

@section('title','Detail Jadwal')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <form method="get" action="{{ route('jadwal.show', $jadwal->id) }}">
                                                @method('get')
                                                @csrf
                                                <div class="form-group row">
                                                    <label for="jadwal" class="col-sm-2 col-form-label">Jadwal</label>
                                                    <div class="col-sm-10">
                                                        <input name="jadwal_id" id="jadwal_id" type="date" class="form-control" placeholder="jadwal_id" value="{{ $jadwal->jadwal_id }}" readonly disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="hari" class="col-sm-2 col-form-label">Hari</label>
                                                    <div class="col-sm-10">
                                                        <input id="hari" type="text" class="form-control" value="{{ $jadwal->hari }}" readonly disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="mapel" class="col-sm-2 col-form-label">Mapel</label>
                                                    <div class="col-sm-10">
                                                        <input id="mapel" type="text" class="form-control" value="{{ $jadwal->mapel->nama_mapel }}" readonly disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="users" class="col-sm-2 col-form-label">Guru</label>
                                                    <div class="col-sm-10">
                                                        <input id="users" type="text" class="form-control" value="{{ $jadwal->users->id }}" readonly disabled>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <form method="get" action="{{ route('jurnal.show', $jurnal->id) }}">
                                                @method('get')
                                                @csrf
                                                <div class="table-responsive">
                                                    <table class="table table-striped thead-border-top-0">
                                                        <thead>
                                                            <tr>
                                                                <th>NISN</th>
                                                                <th>Nama Siswa</th>
                                                                <th>Presensi</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th>{{ $absen->siswa->nisn }}</th>
                                                                <th>{{ $absen->siswa->nama_siswa }}</th>
                                                                <th>{{ $absen->presensi }}</th>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
    
@endsection