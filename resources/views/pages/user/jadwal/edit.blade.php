@extends('layouts.app')

@section('title','Jadwal Pengajar')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-6">
                                            <form method="post" action="{{ route('jadwal.update', $jadwal->id) }}">
                                                @method('put')
                                                @csrf
                                                {{-- <div class="form-group">
                                                    <label for="nama_kelas">Nama Kelas</label>
                                                    <input name="nama_kelas" id="nama_kelas" type="text" class="form-control" value="{{ $jadwal->kelas }}" readonly disabled>
                                                </div> --}}
                                                <div class="form-group">
                                                    <label for="id_mapel">Mapel</label>
                                                    <select name="id_mapel" id="id_mapel" data-toggle="select" class="form-control @error('id_mapel') is-invalid @enderror">
                                                        <option value="">Pilih mapel</option>
                                                        @foreach ($mapel as $m)
                                                            <option value="{{ $m->id }}" @if($pengajar->id_mapel == $m->id) selected @endif>{{ $m->nama_mapel }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="id_kelas">Kelas</label>
                                                    <select name="id_kelas" id="id_kelas" data-toggle="select" class="form-control @error('id_kelas') is-invalid @enderror">
                                                        <option value="">Pilih Kelas</option>
                                                        @foreach ($kelas as $k)
                                                            <option value="{{ $k->id }}" @if($pengajar->id_kelas == $k->id) selected @endif>{{ $k->nama_kelas }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <button type="submit" class="btn btn-primary float-right">Submit</button>                                            
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
    
@endsection