@extends('layouts.app')

@section('title','Jadwal')
    
@section('content')
                <div class="container-fluid page__container mt-4">
                    @include('components.messages')
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ route('jadwal.create') }}" class="btn btn-primary ml-auto mr-3"><i class="fas fa-plus-square"></i></a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row">
                                        <div class="col-md-6 ml-auto mr-2">
                                            <form action="{{ url()->current() }}">
                                                <div class="search-form search-form--light m-2">
                                                    <input name="search" type="text" class="form-control search" placeholder="Search">
                                                    <button class="btn" type="submit" role="button"><i class="material-icons">search</i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped thead-border-top-0">
                                            <thead>
                                                <tr>
                                                    <th>Kelas</th>
                                                    <th>Tanggal Buat</th>
                                                    <th>Tanggal Diperbarui</th>
                                                    <th>Status</th>
                                                    <th style="width: 200px">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($jadwal as $j)
                                                    <tr>
                                                        {{-- <td>{{ $loop->iteration }}</td> --}}
                                                        <td>{{ $j->kelas->nama_kelas }}</td>
                                                        <td>{{ $j->created_at }}</td>
                                                        <td>{{ $j->status }}</td>
                                                        <td>
                                                            <a href="{{ route('jadwal.show', $j->id) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                                            <a href="{{ route('jadwal.edit', $j->id) }}" class="btn btn-success"><i class="far fa-edit"></i></a>
                                                            <form action="{{ route('jadwal.destroy', $j->id) }}" method="post" class="d-inline">
                                                                @method('DELETE')
                                                                @csrf
                                                                <button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    {{ $jadwal->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
@endsection

@section('script')
    
@endsection