@extends('layouts.app')

@section('title','Tambah Siswa')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-6">
                                            <form method="post" action="{{ route('admin.siswa.store') }}">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="nis">NIS</label>
                                                    <input name="nis" id="nis" type="text" class="form-control @error('nis') is-invalid @enderror" value="{{ old('nis') }}" placeholder="NIS">
                                                    @error('nis')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <div class="form-group">
                                                    <label for="nisn">NISN</label>
                                                    <input name="nisn" id="nisn" type="text" class="form-control @error('nisn') is-invalid @enderror" value="{{ old('nisn') }}" placeholder="NISN">
                                                    @error('nisn')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <div class="form-group">
                                                    <label for="siswa">Siswa</label>
                                                    <input name="nama_siswa" id="nama_siswa" type="text" class="form-control @error('nama_siswa') is-invalid @enderror" value="{{ old('nama_siswa') }}" placeholder="Siswa">
                                                    @error('nama_siswa')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <div class="form-group">
                                                    <label for="kelas">Kelas</label>
                                                    <select name="kelas_id" id="kelas_id" data-toggle="select" class="form-control @error('kelas_id') is-invalid @enderror">
                                                        <option value="">Pilih Kelas</option>
                                                        @foreach ($kelas as $k)
                                                            <option value="{{ $k->id }}" @if(old('kelas_id') == $k->id) selected @endif>{{ $k->nama_kelas }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('kelas_id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <div class="form-group">
                                                    <label for="keterangan">Keterangan</label>
                                                    <textarea name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan">{{ old('keterangan') }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="status">Status</label>
                                                    <br>
                                                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                                                        <input type="checkbox" name="status" id="status" value="1" class="custom-control-input">
                                                        <label class="custom-control-label" for="status">Status</label>
                                                    </div>
                                                    <label for="status" class="mb-0">Aktif</label>
                                                </div>
                                                <button type="submit" class="btn btn-primary float-right">Submit</button>                                            
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
    
@endsection