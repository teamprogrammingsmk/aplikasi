@extends('layouts.app')

@section('title','Tambah Jurusan')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-6">
                                            <form method="post" action="{{ route('admin.jurusan.store') }}">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="id">ID Jurusan</label>
                                                    <input name="id" id="id" type="text" class="form-control @error('id') is-invalid @enderror" value="{{ old('id') }}" placeholder="ID Jurusan">
                                                    @error('id')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <div class="form-group">
                                                    <label for="jurusan">Jurusan</label>
                                                    <input name="nama_jurusan" id="nama_jurusan" type="text" class="form-control @error('nama_jurusan') is-invalid @enderror" value="{{ old('nama_jurusan') }}" placeholder="Jurusan">
                                                    @error('nama_jurusan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <button type="submit" class="btn btn-primary float-right">Submit</button>                                            
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
    
@endsection