@extends('layouts.app')

@section('title','Edit Mapel')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-6">
                                            <form method="post" action="{{ route('admin.mapel.update', $mapel->id) }}">
                                                @method('put')
                                                @csrf
                                                <div class="form-group">
                                                    <label for="nama_mapel">Mata Pelajaran</label>
                                                    <input name="nama_mapel" id="nama_mapel" type="text" class="form-control @error('nama_mapel') is-invalid @enderror" value="{{ $mapel->nama_mapel }}" placeholder="Mata Pelajaran">
                                                    @error('nama_mapel')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <div class="form-group">
                                                    <label for="kelompok">Kelompok</label>
                                                    <select name="kelompok" id="kelompok" data-toggle="select" class="form-control @error('kelompok') is-invalid @enderror" style="text-transform: capitalize;">
                                                        <option value="">Pilih kelompok </option>
                                                        @foreach ($kelompok as $k)
                                                            <option value="{{ $k }}" @if($mapel->kelompok == $k) selected @endif>{{ $k }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('kelompok')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <div class="form-group">
                                                    <label for="tingkat">Tingkat</label>
                                                    <select name="tingkat" id="tingkat" data-toggle="select" class="form-control @error('tingkat') is-invalid @enderror" style="text-transform: capitalize;">
                                                        <option value="">Pilih tingkat </option>
                                                        @foreach ($tingkat as $t)
                                                            <option value="{{ $t }}" @if($mapel->tingkat == $t) selected @endif>{{ $t }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('tingkat')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <div class="form-group">
                                                    <label for="jumlah_jam">Jumlah Jam</label>
                                                    <input name="jumlah_jam" id="jumlah_jam" type="text" class="form-control @error('jumlah_jam') is-invalid @enderror" value="{{ $mapel->jumlah_jam }}" placeholder="Jumlah Jam">
                                                    @error('jumlah_jam')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <div class="form-group">
                                                    <label for="jurusan_id">Jurusan</label>
                                                    <select name="jurusan_id" id="jurusan_id" data-toggle="select" class="form-control @error('jurusan_id') is-invalid @enderror">
                                                        <option value="">Pilih Jurusan</option>
                                                        @foreach ($jurusan as $j)
                                                            <option value="{{ $j->id }}" @if($mapel->jurusan_id == $j->id) selected @endif>{{ $j->nama_jurusan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <button type="submit" class="btn btn-primary float-right">Submit</button>                                            
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
    
@endsection