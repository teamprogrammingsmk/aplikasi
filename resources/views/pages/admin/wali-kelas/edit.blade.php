@extends('layouts.app')

@section('title','Edit Wali Kelas')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <form action="{{ route('admin.wali-kelas.update', $waliKelas->id) }}" method="post">
                                        @method('put')
                                        @csrf
                                        <div class="row d-flex justify-content-center">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="kelas_id">Kelas</label>
                                                    <select name="kelas_id" id="kelas_id" class="form-control @error('kelas_id') is-invalid @enderror">
                                                        <option value="">Pilih kelas</option>
                                                        @foreach ($kelas as $kelas)
                                                            <option value="{{ $kelas->id }}" @if($waliKelas->kelas_id == $kelas->id) selected @endif>{{ $kelas->nama_kelas }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('kelas_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror 
                                                </div>
                                                <div class="form-group">
                                                    <label for="user_id">Guru</label>
                                                    <select name="user_id" id="user_id" class="form-control @error('user_id') is-invalid @enderror">
                                                        <option value="">Pilih guru</option>
                                                        @foreach ($guru as $teacher)
                                                            <option value="{{ $teacher->id }}" @if ($waliKelas->user_id == $teacher->id) selected @endif>{{ $teacher->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('user_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror 
                                                </div>
                                                <button class="btn btn-primary float-right">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection