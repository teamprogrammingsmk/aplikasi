@extends('layouts.app')

@section('title','Settings')
    
@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-8">
                                            <form action="{{ route('admin.settings.update') }}" method="post" enctype="multipart/form-data">
                                                @method('put')
                                                @csrf
                                                @foreach ($web_settings as $web_setting)
                                                <div class="form-group row">
                                                    <label for="{{ $web_setting['variable'] }}" class="col-sm-2 col-form-label" style="text-transform: capitalize">{{ str_replace('_', ' ', $web_setting['variable']) }}</label>
                                                    <div class="col-sm-10">
                                                        <input name="{{ $web_setting['variable'] }}" id="{{ $web_setting['variable'] }}" class="form-control" type="{{ $web_setting['type'] }}" @if($web_setting['type'] == 'file') accept="image/*" @endif @if($web_setting['type'] == 'text') value="{{ $web_setting['value'] }}" @endif>
                                                    </div>
                                                </div>
                                                @endforeach
                                                <button type="submit" class="btn btn-primary float-right"> Submit</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
@endsection

@section('script')
    
@endsection