@extends('layouts.app')

@section('title','Edit Guru')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg-8">
                            <form method="post" action="{{ route('admin.guru.update',$guru->id) }}">
                            @method('put')
                            @csrf
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Nama Guru</label>
                                                <input name="name" id="name" type="text" class="form-control @error('name') is-invalid @enderror" value="{{ $guru->name }}" placeholder="Nama Guru">
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror   
                                            </div>
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input  id="username" type="text" class="form-control @error('username') is-invalid @enderror" value="{{ $guru->username }}" placeholder="Username" readonly disabled>
                                                @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror   
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input name="password" id="password" type="text" class="form-control @error('password') is-invalid @enderror"  placeholder="Password">
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md">
                                    <div class="card">
                                        <div class="card-header card-header-large bg-white">
                                            <div class="row d-flex">
                                                <div class="col-md-6">
                                                    <h5 class="m-0">Role</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                @foreach ($roles as $role)
                                                <div class="col-md-6">
                                                    <div class="form-group mb-0">
                                                        <input type="radio" name="role" id="role_{{ $role->id }}" value="{{ $role->name }}" @if($guru->hasRole($role->name)) checked @endif required> 
                                                        <label style="text-transform: capitalize;" for="role_{{ $role->id }}">{{ str_replace('-', ' ', $role->name) }}</label><br />
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header card-header-large bg-white">
                                            <div class="row d-flex">
                                                <div class="col-md-6">
                                                    <h5 class="m-0">Permission</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row d-flex justify-content-center">
                                                @if ($errors->first('permission'))
                                                    <p class="text-danger">{{ $errors->first('permission') }}</p>
                                                @endif
                                                @foreach ($permissions as $key => $permission)
                                                <div class="col-md-6">
                                                    <div class="form-group mb-0">
                                                        <input type="checkbox" name="permission[]" id="permission_{{ $permission->id }}" value="{{ $permission->name }}" @if($guru->hasPermissionTo($permission->name)) checked @endif> 
                                                        <label style="text-transform: capitalize;" for="permission_{{ $permission->id }}">{{ str_replace('-', ' ', $permission->name) }}</label><br />
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Submit</button>                                            
                            </form>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
    
@endsection