@extends('layouts.app')

@section('title','Tambah Pengajar')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-6">
                                            <form method="post" action="{{ route('admin.pengajar.store') }}">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="user_id">Guru</label>
                                                            <select name="user_id" id="user_id" data-toggle="select" class="form-control @error('user_id') is-invalid @enderror">
                                                                <option value="">Pilih guru</option>
                                                                @foreach ($guru as $g)
                                                                    <option value="{{ $g->id }}" @if(old('user_id') == $g->id) selected @endif>{{ $g->name }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('user_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror   
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="mapel_id">Mapel</label>
                                                            <select name="mapel_id" id="mapel_id" data-toggle="select" class="form-control @error('mapel_id') is-invalid @enderror">
                                                                <option value="">Pilih mapel</option>
                                                                @foreach ($mapel as $m)
                                                                    <option value="{{ $m->id }}" @if(old('mapel_id') == $m->id) selected @endif>{{ $m->nama_mapel . ' | ' . $m->tingkat}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('mapel_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror   
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="kelas_id">Kelas</label>
                                                            <select name="kelas_id" id="kelas_id" data-toggle="select" class="form-control @error('kelas_id') is-invalid @enderror">
                                                                <option value="">Pilih kelas</option>
                                                                @foreach ($kelas as $k)
                                                                    <option value="{{ $k->id }}" @if(old('kelas_id') == $k->id) selected @endif>{{ $k->nama_kelas }}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('kelas_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror   
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="tahun_ajar_id">Tahun ajar</label>
                                                            <select name="tahun_ajar_id" id="tahun_ajar_id" data-toggle="select" class="form-control @error('tahun_ajar_id') is-invalid @enderror">
                                                                <option value="">Pilih tahun ajar</option>
                                                                @foreach ($tahunajar as $th)
                                                                    <option value="{{ $th->id }}" @if(old('tahun_ajar_id') == $th->id) selected @endif>{{ $th->periode . '-' . ($th->periode+1)}}</option>
                                                                @endforeach
                                                            </select>
                                                            @error('tahun_ajar_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                            @enderror   
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-primary float-right">Submit</button>                                            
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
    
@endsection