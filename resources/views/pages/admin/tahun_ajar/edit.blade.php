@extends('layouts.app')

@section('title','Edit Tahun Ajar')

@section('content')
                <div class="container-fluid page__container mt-4">
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-header card-header-large bg-white">
                                    <div class="row d-flex">
                                        <div class="col-md-6">
                                            <h5 class="m-0">@yield('title')</h5>
                                        </div>
                                        <a href="{{ url()->previous() }}" class="btn btn-danger ml-auto mr-3">Kembali</a>
                                    </div>
                                </div>
                                <div class="card-body button-list">
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-md-6">
                                            <form method="post" action="{{ route('admin.tahun-ajar.update', $tahun_ajar->id) }}">
                                                @method('put')
                                                @csrf
                                                <div class="form-group">
                                                    <label for="periode">Periode</label>
                                                    <input name="periode" id="periode" type="number" class="form-control @error('periode') is-invalid @enderror" value="{{ $tahun_ajar->periode }}" placeholder="Periode">
                                                    @error('periode')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror   
                                                </div>
                                                <button type="submit" class="btn btn-primary float-right">Submit</button>                                            
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
@endsection

@section('script')
    
@endsection