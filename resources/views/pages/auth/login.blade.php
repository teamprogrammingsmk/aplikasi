@extends('layouts.auth')

@section('title','Login')

@section('content')

<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="form-group">
        <label class="text-label" for="username">Username</label>
        <div class="input-group input-group-merge">
            <input name="username" id="username" type="text" class="form-control form-control-prepended @error('username') is-invalid @enderror" placeholder="Username" autocomplete="username" autofocus required>
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="far fa-envelope"></span>
                </div>
            </div>
            @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror                                                                                                  
        </div>
    </div>
    <div class="form-group">
        <label class="text-label" for="password">Password</label>
        <div class="input-group input-group-merge">
            <input name="password" id="password" type="password" class="form-control form-control-prepended @error('password') is-invalid @enderror" placeholder="Enter your password">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-key"></span>
                </div>
            </div>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror   
        </div>
    </div>
    <br>
    <div class="form-group text-center">
        <button class="btn btn-primary mb-3" type="submit">Login</button>
    </div>
</form>
<p class="mb-0 text-center">Made with <i class="fas fa-heart text-danger"></i> by <a href="{{ route('author') }}">RPL 2018</a></p>

@endsection