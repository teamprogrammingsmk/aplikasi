<div class="mdk-drawer  js-mdk-drawer" id="default-drawer" data-align="start">
    <div class="mdk-drawer__content">
        <div class="sidebar sidebar-light sidebar-left simplebar" data-simplebar>
            <div class="d-flex align-items-center sidebar-p-a border-bottom sidebar-account">
                <a href="{{ route('user.index') }}"
                    class="flex d-flex align-items-center text-underline-0 text-body">
                    <span class="avatar mr-3">
                        <img src="{{ asset('images/avatar/demi.png') }}" alt="avatar"
                            class="avatar-img rounded-circle">
                    </span>
                    <span class="flex d-flex flex-column">
                        <strong>{{ auth()->user()->name }}</strong>
                        <small class="text-muted text-uppercase">{{ str_replace('-', ' ', auth()->user()->getRoleNames()[0]) }}</small>
                    </span>
                </a>
                <div class="dropdown ml-auto">
                <a href="#" data-toggle="dropdown" data-caret="false" class="text-muted"><i class="material-icons">more_vert</i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ route('user.index') }}">Edit profile</a>
                        <a class="dropdown-item" href="{{ route('user.password') }}">Edit password</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                    </div>
                </div>
            </div>
            <div class="sidebar-heading sidebar-m-t">Menu</div>
            <ul class="sidebar-menu">
                <ul class="sidebar-menu" id="components_menu">
                    <li class="sidebar-menu-item">
                        <a class="sidebar-menu-button" href="{{ route('home') }}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">home</i>
                            <span class="sidebar-menu-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="sidebar-menu-item">
                        <a class="sidebar-menu-button" data-toggle="collapse" href="#jurnal">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">book</i>
                            <span class="sidebar-menu-text">Jurnal</span>
                            <span class="ml-auto sidebar-menu-toggle-icon"></span>
                        </a>
                        <ul class="sidebar-submenu collapse" id="jurnal">
                            @can('see-jurnal')
                            <li class="sidebar-menu-item">
                                <a class="sidebar-menu-button" href="{{ route('jurnal.index') }}">
                                    <span class="sidebar-menu-text">Jurnal</span>
                                </a>
                            </li>
                            @endcan
                            @can('see-jurnal')
                            <li class="sidebar-menu-item">
                                <a class="sidebar-menu-button" href="{{ route('jurnal.all') }}">
                                    <span class="sidebar-menu-text">All Jurnal</span>
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                    {{-- @can('see-laporan')
                    <li class="sidebar-menu-item">
                        <a class="sidebar-menu-button" href="{{ route('laporan_sdm.index') }}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">book</i>
                            <span class="sidebar-menu-text">Laporan</span>
                        </a>
                    </li>
                    @endcan --}}
                    @can('see-laporan')
                    <div class="sidebar-block p-0">
                        <ul class="sidebar-menu" id="components_menu">
                            <li class="sidebar-menu-item">
                                <a class="sidebar-menu-button" data-toggle="collapse" href="#laporan">
                                    <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">book</i>
                                    <span class="sidebar-menu-text">Laporan</span>
                                    <span class="ml-auto sidebar-menu-toggle-icon"></span>
                                </a>
                                <ul class="sidebar-submenu collapse" id="laporan">
                                    @can('see-laporan-sdm')
                                    <li class="sidebar-menu-item">
                                        <a class="sidebar-menu-button" href="{{ route('laporan_sdm.index') }}">
                                            <span class="sidebar-menu-text">Laporan SDM</span>
                                        </a>
                                    </li>
                                    @endcan
                                    @can('see-laporan-bk')
                                    <li class="sidebar-menu-item">
                                        <a class="sidebar-menu-button" href="{{ route('laporan_bk.index') }}">
                                            <span class="sidebar-menu-text">Laporan BK</span>
                                        </a>
                                    </li>
                                    @endcan
                                    {{-- @can('see-laporan-sdm')
                                    <li class="sidebar-menu-item">
                                        <a class="sidebar-menu-button" href="{{ route('laporan_sdm.all') }}">
                                            <span class="sidebar-menu-text">All Laporan SDM</span>
                                        </a>
                                    </li>
                                    @endcan --}}
                                </ul>
                            </li>
                        <ul>
                    </div>
                    @endcan
                </ul>
            </ul>
            @if (App\Models\WaliKelas::userHasWaliKelas(auth()->user()->id))
            <div class="sidebar-heading">Wali Kelas</div>
                <div class="sidebar-block p-0">
                    <ul class="sidebar-menu" id="components_menu">
                        <li class="sidebar-menu-item">
                            <a class="sidebar-menu-button" href="{{ route('wali-kelas.index') }}">
                                <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">school</i>
                                <span class="sidebar-menu-text">Wali Kelas</span>
                            </a>
                        </li>
                    </ul>
                </div>
            @endif
            @role('super-admin|admin')
                <div class="sidebar-heading">Admin Menu</div>
                <div class="sidebar-block p-0">
                    <ul class="sidebar-menu" id="components_menu">
                        @if(auth()->user()->can('see-kelas') || auth()->user()->can('see-siswa') || auth()->user()->can('see-guru') || auth()->user()->can('see-mapel') || auth()->user()->can('see-jurusan') || auth()->user()->can('see-tahun-ajar'))
                        <li class="sidebar-menu-item">
                            <a class="sidebar-menu-button" data-toggle="collapse" href="#master">
                                <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">dvr</i>
                                <span class="sidebar-menu-text">Master</span>
                                <span class="ml-auto sidebar-menu-toggle-icon"></span>
                            </a>
                            <ul class="sidebar-submenu collapse" id="master">
                                @can('see-kelas')
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="{{ route('admin.kelas.index') }}">
                                        <span class="sidebar-menu-text">Kelas</span>
                                    </a>
                                </li>
                                @endcan
                                @can('see-siswa')
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="{{ route('admin.siswa.index') }}">
                                        <span class="sidebar-menu-text">Siswa</span>
                                    </a>
                                </li>
                                @endcan
                                @can('see-guru')
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="{{ route('admin.guru.index') }}">
                                        <span class="sidebar-menu-text">Guru</span>
                                    </a>
                                </li>
                                @endcan
                                @can('see-mapel')
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="{{ route('admin.mapel.index') }}">
                                        <span class="sidebar-menu-text">Mapel</span>
                                    </a>
                                </li>
                                @endcan
                                @can('see-jurusan')
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="{{ route('admin.jurusan.index') }}">
                                        <span class="sidebar-menu-text">Jurusan</span>
                                    </a>
                                </li>
                                @endcan
                                @can('see-tahun-ajar')
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="{{ route('admin.tahun-ajar.index') }}">
                                        <span class="sidebar-menu-text">Tahun Ajar</span>
                                    </a>
                                </li>
                                @endcan
                                <li class="sidebar-menu-item">
                                    <a class="sidebar-menu-button" href="{{ route('admin.wali-kelas.index') }}">
                                        <span class="sidebar-menu-text">Wali Kelas</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="sidebar-menu-item">
                            <a class="sidebar-menu-button" href="{{ route('admin.pengajar.index') }}">
                                <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">people</i>
                                <span class="sidebar-menu-text">Pengajar</span>
                            </a>
                        </li>
                        <li class="sidebar-menu-item">
                            <a class="sidebar-menu-button" href="{{ route('admin.settings.index') }}">
                                <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">settings</i>
                                <span class="sidebar-menu-text">Settings</span>
                            </a>
                        </li>
                    </ul>
                </div>
            @endrole
        </div>
    </div>
    
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>