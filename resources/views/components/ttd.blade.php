<style>
    .surat-footer-box {
        position: relative;
    }

    .kepala-sekolah {
        position: absolute;
        top: 10vh;
        margin-top: 21px;
        margin-left: 13%;
        font-size: 18px;
    }

    .kepala-sekolah>p {
        margin-top: 5em;
        font-size: 11pt;
    }

    .guru {
        position: absolute;
        top: 10vh;
        margin-left: 68%;
        font-size: 18px;
    }

    .guru>p {
        margin-top: 5em;
        font-size: 11pt;
    }

    .prefix {
        clear: both;
    }
</style>

<div class="surat-footer-box">
    <div class="kepala-sekolah">
        <font size="3">Mengetahui,</font><br />
        <font size="3">KEPALA SEKOLAH</font><br />
        <p><b> SUPRIHARTONO, S.Pd, M.M </b><br /> Pembina Tk. I <br /> NIP. 19621128 198803 1 010 </p>
    </div>
    <div class="prefix"></div>
    <div class="guru">
        <font size="3">Situbondo, {{ $tanggalsampaicustomttd}}</font><br />
        <font size="3">Guru Mata Pelajaran</font><br /><br />
        <?php $query=is_null($user->nip)?'-':$user->nip;?>
        <p><b> {{ $user->name }} </b><br />NIP. {{$query}}</p>
    </div>
</div>
