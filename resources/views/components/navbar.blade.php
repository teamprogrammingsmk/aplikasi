<!-- Header -->

        <div id="header" class="mdk-header js-mdk-header m-0" data-fixed>
            <div class="mdk-header__content">

                <div class="navbar navbar-expand-sm navbar-main navbar-dark bg-dark  pr-0" id="navbar" data-primary>
                    <div class="container-fluid p-0">

                        <!-- Navbar toggler -->

                        <button class="navbar-toggler navbar-toggler-right d-block d-md-none" type="button" data-toggle="sidebar">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <!-- Navbar Brand -->
                        <a href="{{ route('home') }}" class="navbar-brand ">
                            <img class="navbar-brand-icon" src="{{ asset('storage/images/logo/'. App\Models\Setting::setting()['web_logo']) }}" width="32" alt="Logo">
                            <span>{{ empty(App\Models\Setting::setting()) ? config('app.name') : App\Models\Setting::setting()['web_title'] }}</span>
                        </a>

                        <ul class="nav navbar-nav d-none d-sm-flex border-left navbar-height align-items-center">
                            <li class="nav-item dropdown">
                                <a href="#account_menu" class="nav-link dropdown-toggle" data-toggle="dropdown" data-caret="false">
                                    <img src="{{ asset('images/avatar/demi.png') }}" class="rounded-circle" width="32" alt="Frontted">
                                    <span class="ml-1 d-flex-inline">
                                        <span class="text-light">{{ auth()->user()->name }}</span>
                                    </span>
                                </a>
                                <div id="account_menu" class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="{{ route('user.index') }}">Edit profile</a>
                                    <a class="dropdown-item" href="{{ route('user.password') }}">Edit password</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <!-- // END Header -->