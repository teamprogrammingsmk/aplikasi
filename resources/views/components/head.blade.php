    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title') - {{ empty(App\Models\Setting::setting()) ? config('app.name') : App\Models\Setting::setting()['web_title'] }}</title>
    <link rel="shortcut icon" href="{{ asset('storage/images/logo/'. App\Models\Setting::setting()['web_logo']) }}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    
    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- Simplebar -->
    <link type="text/css" href="{{ asset('vendor/simplebar.min.css') }}" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/app.rtl.css') }}" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{ asset('css/vendor-material-icons.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/vendor-material-icons.rtl.css') }}" rel="stylesheet">

    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href="{{ asset('css/vendor-fontawesome-free.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/vendor-fontawesome-free.rtl.css') }}" rel="stylesheet">

    <!-- Select2 -->
    <link type="text/css" href="{{ asset('css/vendor-select2.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/vendor-select2.rtl.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet">

    <!-- Flatpickr -->
    <link type="text/css" href="{{ asset('css/vendor-flatpickr.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/vendor-flatpickr.rtl.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/vendor-flatpickr-airbnb.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('css/vendor-flatpickr-airbnb.rtl.css') }}" rel="stylesheet">
    
    <!-- Datatables -->
    <link type="text/css" href="{{ asset('datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('datatables/css/responsive.bootstrap4.min.css') }}" rel="stylesheet">

    <!-- My Custom CSS -->
    <link type="text/css" href="{{ asset('css/hello-jurnal.css') }}" rel="stylesheet">

    @yield('css')