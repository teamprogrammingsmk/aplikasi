<!-- jQuery -->
<script src="{{ asset('vendor/jquery.min.js') }}"></script>

<!-- Bootstrap -->
<script src="{{ asset('vendor/popper.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap.min.js') }}"></script>

<!-- Simplebar -->
<script src="{{ asset('vendor/simplebar.min.js') }}"></script>

<!-- DOM Factory -->
<script src="{{ asset('vendor/dom-factory.js') }}"></script>

<!-- MDK -->
<script src="{{ asset('vendor/material-design-kit.js') }}"></script>

<!-- App -->
<script src="{{ asset('js/toggle-check-all.js') }}"></script>
<script src="{{ asset('js/check-selected-row.js') }}"></script>
<script src="{{ asset('js/dropdown.js') }}"></script>
<script src="{{ asset('js/sidebar-mini.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

<!-- Datatables -->
<script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('datatables/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('datatables/js/responsive.bootstrap4.min.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
<script src="{{ asset('js/select2.js') }}"></script>

<!-- Flatpickr -->
<script src="{{ asset('vendor/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('js/flatpickr.js') }}"></script>

<!-- Typed.js -->
<script src="{{ asset('js/typed.js') }}"></script>

<script src="{{ $cdn ?? asset('vendor/sweetalert/sweetalert.all.js')  }}"></script>

@yield('script')