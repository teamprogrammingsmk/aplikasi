<div class="table-responsive">
  <table class="table table-striped" align="center">
      <tr>
        <th><img width="75px" height="auto" src="{{ asset('storage/images/logo/jatim.png') }}" ></th>
        <td class="text-center">
          <center>
            <font size="3">PEMERINTAH PROVINSI JAWA TIMUR</font><br>
            <font size="3">DINAS PENDIDIKAN</font><br>
            <font size="4"><b>SEKOLAH MENENGAH KEJURUAN NEGERI 1 PANJI</b></font><br>
            <font size="2"><i>Jl. Gunung Arjuno 17 Tlp/Fax (0338) 672507/ 67732 Email : <u>smkn1panji@yahoo.com</u></i></font><br>
            <font size="4"><b><u>S I T U B O N D O</u></b>&nbsp; <span style="font-size: 14px">68322</span></font>
          </center>
        </td>        
        <th><img width="68px" height="auto" src="{{ asset('storage/images/logo/' . App\Models\Setting::setting()['web_logo']) }}" alt=""></>
      </tr>
  </table>
</div>