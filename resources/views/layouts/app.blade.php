<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('components.head')
</head>

<body  class="layout-default">

    <div class="preloader"></div>
    @include('sweetalert::alert')
    <!-- Header Layout -->
    <div class="mdk-header-layout js-mdk-header-layout">
        @include('components.navbar')
        <!-- Header Layout Content -->
        <div class="mdk-header-layout__content">

            <div class="mdk-drawer-layout js-mdk-drawer-layout" data-push data-responsive-width="992px">
                <div class="mdk-drawer-layout__content page">
                    @yield('content')
                </div>
                <!-- // END drawer-layout__content -->
            @include('components.sidebar')
            </div>
            <!-- // END drawer-layout -->

        </div>
        <!-- // END header-layout__content -->
    </div>
    
    @include('components.script')
    @yield('modal')
</body>

</html>