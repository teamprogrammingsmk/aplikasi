<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('components.head')
</head>

<body class="layout-login-centered-boxed">

    <div class="layout-login-centered-boxed__form card">
        <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-5 navbar-light">
            <a href="{{ route("home") }}" class="navbar-brand flex-column mb-2 align-items-center mr-0" style="min-width: 0">
                <img class="navbar-brand-icon mr-0 mb-2" src="{{ asset('storage/images/logo/' . App\Models\Setting::setting()['web_logo']) }}" width="75" alt="Logo">
                <span>{{ empty(App\Models\Setting::setting()) ? config('app.name') : App\Models\Setting::setting()['web_title'] }}</span>
            </a>
        </div>
        @yield('content')
        <!-- login.blade.php -->
    </div>


    @include('components.script')
</body>

</html>