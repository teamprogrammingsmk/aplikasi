<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Setting;
use App\Models\Siswa;
use App\Models\Mapel;
use App\Models\Jurnal;
use App\Models\Absen;
use App\Models\Bulan;
use App\Models\Pengajar;
use App\User;
use Dompdf\Dompdf;
use App\Models\TahunAjar;
use DB;
use Carbon\Carbon;

class LaporanController extends Controller
{    
    public function indexsdm(Request $request)
    {
        $pilihantahunajar = $request->get('periode');
        $pilihanguru = $request->get('guru');
        $tanggaldari = $request->get('tanggaldari');
        $tanggalsampai = $request->get('tanggalsampai');
        $tahunajar = TahunAjar::get();
        $user = User::get();
        $settings= Setting::setting();
        $bulan = Bulan::getBulan();
        $jurnal = Jurnal::get();
        $dasta = Absen::join('jurnal', 'jurnal.id', '=', 'absen.jurnal_id')
        ->join('siswa', 'siswa.id', '=', 'absen.siswa_id')
        ->join('users', 'users.id', '=', 'jurnal.user_id')
        ->join('pengajar', 'pengajar.user_id', '=', 'users.id')
        ->join('mapel', 'mapel.id', '=', 'jurnal.mapel_id')
        ->join('kelas', 'kelas.id', '=', 'jurnal.kelas_id')
        ->join('tahun_ajar', 'tahun_ajar.id', '=', 'pengajar.tahun_ajar_id')
        ->orWhere('users.id','=', $pilihanguru)
        ->where('tahun_ajar.periode','=',$pilihantahunajar)
        ->whereBetween('jurnal.tanggal', [$tanggaldari, $tanggalsampai])
        ->select('jurnal.tanggal','mapel.nama_mapel','kelas.nama_kelas','jurnal.materi','absen.presensi','absen.jurnal_id','jurnal.id')
        ->groupBy('jurnal.id')
        ->paginate(10);
        
        $data=[];
        foreach($dasta as $key => $siswa) {
            $data[$key]['materi']= $siswa->materi;
            $data[$key]['kelas'] = $siswa->nama_kelas;
            $data[$key]['tanggal'] = \Carbon\Carbon::parse($siswa->tanggal)->format('d-m-Y');         
            $data[$key]['mapel'] = $siswa->nama_mapel;            
            $data[$key]['jurnal'] = $siswa->materi;          
            $data[$key]['idjurnal'] = $siswa->id;          
            $data[$key]['jurnalid'] = $siswa->jurnal_id;          
            $data[$key]['absen']['hadir'] = Absen::join('jurnal', 'absen.jurnal_id', '=', 'jurnal.id')->where('absen.presensi','=','hadir')->where('absen.jurnal_id', '=', $siswa->id)->count();
            $data[$key]['absen']['ijin'] = Absen::join('jurnal', 'absen.jurnal_id', '=', 'jurnal.id')->where('absen.presensi','=','ijin')->where('absen.jurnal_id', '=', $siswa->id)->count();
            $data[$key]['absen']['sakit'] = Absen::join('jurnal', 'absen.jurnal_id', '=', 'jurnal.id')->where('absen.presensi','=','sakit')->where('absen.jurnal_id', '=', $siswa->id)->count();
            $data[$key]['absen']['alpha'] = Absen::join('jurnal', 'absen.jurnal_id', '=', 'jurnal.id')->where('absen.presensi','=','alpha')->where('absen.jurnal_id', '=', $siswa->id)->count();
        }
        return view('pages.user.laporan.laporan_sdm.index', compact('settings','data','dasta','bulan','tahunajar' ,'user','pilihantahunajar','pilihanguru','tanggaldari','tanggalsampai'));
    }

    public function indexbk(Request $request)
    {
        
        
        $bulan = Bulan::getBulan();
        $kelas = Kelas::get();
        $jurnal = Jurnal::get();

        $pilihkelas = $request->get('kelas');

        $absen = Absen::with([
            'jurnal' => function($query) {
                $query->where('jurnal.kelas_id', $_GET['kelas']);
                $query->whereBetween('jurnal.tanggal', ['2020-08-01', '2020-08-30']);
            },
        ])->get();
        // $pilihankelas = $request->get('kelas');
        // $tanggaldari = $request->get('tanggaldari');
        // $tanggalsampai = $request->get('tanggalsampai');
        // $pilihanbulan = $request->get('bulan');
        // $settings= Setting::setting();
        // $bulan = Bulan::getBulan();
        // $kelas = Kelas::get();
        // $jurnal = Jurnal::get();
        // $dastabk = Absen::select('siswa.nama_siswa','absen.presensi','jurnal.id')
        // ->join('jurnal', 'jurnal.id', '=', 'absen.jurnal_id')
        // ->join('siswa', 'siswa.id', '=', 'absen.siswa_id')
        // ->join('kelas', 'kelas.id', '=', 'jurnal.kelas_id')
        // ->where('kelas.id', $pilihankelas)
        // ->orwhereBetween('jurnal.tanggal', [$tanggaldari, $tanggalsampai])
        // ->whereMonth('jurnal.tanggal'   )
        // ->groupBy('jurnal.id')
        // ->paginate(38);
        // $databk=[];
        // foreach($dastabk as $key => $siswa) {
        //     $databk[$key]['nama_siswa'] = $siswa->nama_siswa;          
        // }
        return view('pages.user.laporan.laporan_bk.index', compact('absen', 'kelas', 'bulan'));
    }

    public function print(Request $request)
    {
        $pilihantahunajar = $request->get('periode');
        $pilihanguru = $request->get('guru');
        $tanggaldari = $request->get('tanggaldari');
        $tanggalsampai = $request->get('tanggalsampai');
        $tanggaldaricustom = \Carbon\Carbon::parse($tanggaldari)->format('d-m-Y');
        $tanggalsampaicustom = \Carbon\Carbon::parse($tanggalsampai)->format('d-m-Y');
        $tanggalsampaicustomttd = \Carbon\Carbon::parse($tanggalsampai)->isoFormat('D MMMM Y');
        $tahunajar = TahunAjar::get();
        $user = User::findOrFail($pilihanguru);
        $settings= Setting::setting();
        $bulan = Bulan::getBulan();
        //JOIN
        $dasta = Absen::join('jurnal', 'jurnal.id', '=', 'absen.jurnal_id')
        ->join('siswa', 'siswa.id', '=', 'absen.siswa_id')
        ->join('users', 'users.id', '=', 'jurnal.user_id')
        ->join('pengajar', 'pengajar.user_id', '=', 'users.id')
        ->join('mapel', 'mapel.id', '=', 'jurnal.mapel_id')
        ->join('kelas', 'kelas.id', '=', 'jurnal.kelas_id')
        ->join('tahun_ajar', 'tahun_ajar.id', '=', 'pengajar.tahun_ajar_id')
        ->where('users.id','=', $pilihanguru)
        ->where('tahun_ajar.periode','=',$pilihantahunajar)
        ->whereBetween('jurnal.tanggal', [$tanggaldari, $tanggalsampai])
        ->select('jurnal.tanggal','mapel.nama_mapel','kelas.nama_kelas','jurnal.materi','absen.presensi','absen.jurnal_id','jurnal.id')
        ->groupBy('jurnal.id')
        ->get();

        $data=[];
        foreach($dasta as $key => $siswa) {
            $data[$key]['materi']= $siswa->materi;
            $data[$key]['kelas'] = $siswa->nama_kelas;
            $data[$key]['tanggal'] = \Carbon\Carbon::parse($siswa->tanggal)->format('d-m-Y');           
            $data[$key]['mapel'] = $siswa->nama_mapel;            
            $data[$key]['jurnal'] = $siswa->materi;          
            $data[$key]['idjurnal'] = $siswa->id;          
            $data[$key]['jurnalid'] = $siswa->jurnal_id;         
            $data[$key]['nip'] = $siswa->nip;         
            $data[$key]['name'] = $siswa->name;         
            $data[$key]['absen']['hadir'] = Absen::join('jurnal', 'absen.jurnal_id', '=', 'jurnal.id')->where('absen.presensi','=','hadir')->where('absen.jurnal_id', '=', $siswa->id)->count();
            $data[$key]['absen']['ijin'] = Absen::join('jurnal', 'absen.jurnal_id', '=', 'jurnal.id')->where('absen.presensi','=','ijin')->where('absen.jurnal_id', '=', $siswa->id)->count();
            $data[$key]['absen']['sakit'] = Absen::join('jurnal', 'absen.jurnal_id', '=', 'jurnal.id')->where('absen.presensi','=','sakit')->where('absen.jurnal_id', '=', $siswa->id)->count();
            $data[$key]['absen']['alpha'] = Absen::join('jurnal', 'absen.jurnal_id', '=', 'jurnal.id')->where('absen.presensi','=','alpha')->where('absen.jurnal_id', '=', $siswa->id)->count();
        }
        return view('pages.user.laporan.print.sdm', compact('settings','pilihantahunajar','pilihanguru','tanggaldari','tanggalsampai','bulan','user','data','tanggaldaricustom','tanggalsampaicustom','tanggalsampaicustomttd'));
    }
}