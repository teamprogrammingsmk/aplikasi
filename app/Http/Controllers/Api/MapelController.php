<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mapel;

class MapelController extends Controller
{
    public function getMapel(Request $request) {
        $mapel = Mapel::where('id', $request->id)->first();

        return response()->json([
            'status' => true,
            'data' => $mapel
        ], 200);
    }
}
