<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Siswa;

class SiswaController extends Controller {

    public function login(Request $request)
    {
        $nis = $request->nis;
        $password = $request->password;

        $student = Siswa::where('nis', $nis)->first();

        if(!is_null($student) && $password == $nis . '*') {
            $student->toArray();

            return response()->json([
                "status" => true,
                "data" => [
                    "id" => $student["id"],
                    "name" => $student["nama_siswa"],
                    "kelas" => [
                        "kelas_id" => $student["kelas"]["id"],
                        "kelas" => $student["kelas"]["nama_kelas"]
                    ],
                    "role" => "siswa"
                ],
            ], 200);
        } else {
            return response()->json([
                "status" => false,
                "data" => [
                    "message" => "Invalid Credentials"
                ]
            ], 401);
        }
    }
}