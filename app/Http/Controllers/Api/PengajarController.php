<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengajar;

class PengajarController extends Controller
{
    public function getPengajar(Request $request) {
        $pengajar = Pengajar::with(['kelas', 'mapel'])->where('user_id', $request->id)->get();

        return response()->json([
            'status' => true,
            'data' => $pengajar
        ], 200);
    }
}
