<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User, App\Models\Pengajar;
use Auth;

class UserController extends Controller {

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        $user = User::where('username', $request->only('username'))->first()->toArray();

        if(Auth::attempt($credentials)) {
            return response()->json([
                'status' => true,
                'data' => [
                    'id' => $user['id'],
                    'nip' => $user['nip'],
                    'name' => $user['name'],
                    'pangkat' => $user['pangkat'],
                    'username' => $user['username'],
                    'role' => 'admin'
                ],
            ], 200);
        } else {
            return response()->json([
                "status" => false,
                "data" => [
                    "message" => "Invalid Credentials"
                ]
            ], 401);
        }
    }
}