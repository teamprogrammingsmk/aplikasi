<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Mapel\MapelCreateUpdateRequest;
use App\Models\Mapel;
use App\Models\Jurusan;
use DataTables;
use Alert;

class MapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $mapel = Mapel::with('jurusan')->get();
            return DataTables::of($mapel)
                            ->addColumn('action', function($mapel) {
                                $button = "<a href=".route('admin.mapel.edit', $mapel->id)." class='btn btn-success'><i class='far fa-edit'></i></a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<button class='btn btn-danger' data-remote='".route('admin.mapel.destroy', $mapel->id)."'><i class='fas fa-trash-alt'></i></button>";
                                return $button;
                            })
                            ->rawColumns(['action'])
                            ->make(true);
        }


        return view('pages.admin.mapel.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelompok = Mapel::getPossibleEnumValues('kelompok');
        $tingkat = Mapel::getPossibleEnumValues('tingkat');
        $jurusan = Jurusan::get();

        return view('pages.admin.mapel.create', compact('kelompok','tingkat','jurusan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MapelCreateUpdateRequest $request)
    {
        $mapel = new Mapel;
        $mapel->nama_mapel = $request->nama_mapel;
        $mapel->kelompok = $request->kelompok;
        $mapel->tingkat = $request->tingkat;
        $mapel->keterangan = $request->keterangan;
        $mapel->jumlah_jam = $request->jumlah_jam;
        $mapel->jurusan_id = $request->jurusan_id;
        $mapel->save();

        Alert::success('Sukses!', 'Data berhasil ditambahkan');
        return redirect(route('admin.mapel.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mapel = Mapel::findOrFail($id);
        $kelompok = Mapel::getPossibleEnumValues('kelompok');
        $tingkat = Mapel::getPossibleEnumValues('tingkat');
        $jurusan = Jurusan::get();

        return view('pages.admin.mapel.edit', compact('mapel','kelompok','tingkat','jurusan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MapelCreateUpdateRequest $request, $id)
    {
        $mapel = Mapel::findOrFail($id);
        $mapel->nama_mapel = $request->nama_mapel;
        $mapel->kelompok = $request->kelompok;
        $mapel->tingkat = $request->tingkat;
        $mapel->jumlah_jam = $request->jumlah_jam;
        $mapel->jurusan_id = $request->jurusan_id;
        $mapel->save();

        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(route('admin.mapel.index'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mapel = Mapel::findOrFail($id);
        $mapel->delete();

        Alert::success('Sukses!', 'Data berhasil dihapus');
        return redirect(route('admin.mapel.index'));
    }
}
