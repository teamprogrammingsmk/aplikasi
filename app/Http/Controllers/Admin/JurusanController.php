<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Jurusan\JurusanCreateRequest, App\Http\Requests\Jurusan\JurusanUpdateRequest;
use App\Models\Jurusan;
use DataTables;
use Alert;

class JurusanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $jurusan = Jurusan::get();
            return DataTables::of($jurusan)
                            ->addColumn('action', function($jurusan) {
                                $button = "<a href=".route('admin.jurusan.edit', $jurusan->id)." class='btn btn-success'><i class='far fa-edit'></i></a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<button class='btn btn-danger' data-remote='".route('admin.jurusan.destroy', $jurusan->id)."'><i class='fas fa-trash-alt'></i></button>";
                                return $button;
                            })
                            ->rawColumns(['action'])
                            ->make(true);
        }

        return view('pages.admin.jurusan.index');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('pages.admin.jurusan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JurusanCreateRequest $request)
    {
        $jurusan = new Jurusan;
        $jurusan->id = $request->id;
        $jurusan->nama_jurusan = $request->nama_jurusan;
        $jurusan->save();

        Alert::success('Sukses!', 'Data berhasil ditambahkan');
        return redirect(route('admin.jurusan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jurusan = Jurusan::findOrFail($id);

        return view('pages.admin.jurusan.edit', compact('jurusan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JurusanUpdateRequest $request, $id)
    {
        $jurusan = Jurusan::findOrFail($id);
        $jurusan->nama_jurusan = $request->nama_jurusan;
        $jurusan->save();

        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(route('admin.jurusan.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jurusan = Jurusan::findOrFail($id);
        $jurusan->delete();

        Alert::success('Sukses!', 'Data berhasil dihapus');
        return redirect(route('admin.jurusan.index'));
    }
}
