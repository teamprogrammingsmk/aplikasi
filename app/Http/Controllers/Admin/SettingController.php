<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use Illuminate\Support\Facades\Storage;
use Alert;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $web_settings = Setting::get();

        return view('pages.admin.settings.index', compact('web_settings'));
    }

    public function update(Request $request) {
        $updates = $request->all();
        
        foreach($updates as $key => $value) {
            if($key == 'web_logo') {
                $file = $request->file($key);
                $validatedData = $request->validate([
                    'web_logo' => 'image|max:2048|mimes:jpeg,jpg,png'
                ]);    
                $settings = Setting::where('variable', $key)->first();
                unlink(public_path('storage/images/logo/'.$settings['value']));
                $filename = $file->getClientOriginalName();
                $file->storeAs('images/logo', $filename, 'public');
                $value = $filename;
            }
            Setting::where('variable',$key)->update(['value' => $value]);
        }

        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(url()->previous());
    }
}
