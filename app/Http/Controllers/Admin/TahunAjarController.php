<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\TahunAjar\TahunAjarRequest;
use App\Models\TahunAjar;
use DataTables;
use Alert;

class TahunAjarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $tahun_ajar = TahunAjar::get();
            return DataTables::of($tahun_ajar)
                            ->addColumn('action', function($tahun_ajar) {
                                $button = "<a href=".route('admin.tahun-ajar.edit', $tahun_ajar->id)." class='btn btn-success'><i class='far fa-edit'></i></a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<button class='btn btn-danger' data-remote='".route('admin.tahun-ajar.destroy', $tahun_ajar->id)."'><i class='fas fa-trash-alt'></i></button>";
                                return $button;
                            })
                            ->rawColumns(['materi','tanggal','action'])
                            ->make(true);
        }

        return view('pages.admin.tahun_ajar.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.tahun_ajar.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TahunAjarRequest $request)
    {
        TahunAjar::create([
            'periode' => $request->periode,
        ]);
        
        Alert::success('Sukses!', 'Data berhasil ditambahkan');
        return redirect(route('admin.tahun-ajar.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tahun_ajar = TahunAjar::findOrFail($id);

        return view('pages.admin.tahun_ajar.edit', compact('tahun_ajar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TahunAjarRequest $request, $id)
    {
        $tahun_ajar = TahunAjar::findOrFail($id);
        $tahun_ajar->periode = $request->periode;
        $tahun_ajar->save();

        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(route('admin.tahun-ajar.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tahun_ajar = TahunAjar::findOrFail($id);
        $tahun_ajar->delete();
        
        Alert::success('Sukses!', 'Data berhasil dihapus');
        return redirect(route('admin.tahun-ajar.index'));
    }
}
