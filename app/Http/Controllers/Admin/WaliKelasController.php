<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\WaliKelas\WaliKelasRequest;
use App\Models\WaliKelas;
use App\Models\Kelas;
use App\User;
use DataTables;
use Alert;

class WaliKelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $waliKelas = WaliKelas::with('user', 'kelas')->get();
            return DataTables::of($waliKelas)
                            ->addColumn('action', function($waliKelas) {
                                $button = "<button class='btn btn-danger' data-remote='".route('admin.wali-kelas.destroy', $waliKelas->id)."'><i class='fas fa-trash-alt'></i></button>";
                                return $button;
                            })
                            ->rawColumns(['action'])
                            ->make(true);
        }

        return view('pages.admin.wali-kelas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = Kelas::get();
        $guru = User::get();

        return view('pages.admin.wali-kelas.create', compact('kelas', 'guru'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WaliKelasRequest $request)
    {
        try {
            $waliKelas = new WaliKelas;
            $waliKelas->user_id = $request->user_id;
            $waliKelas->kelas_id = $request->kelas_id;
            $waliKelas->save();

            Alert::success('Sukses!', 'Data berhasil ditambahkan');
            return redirect(route('admin.wali-kelas.index'));
        } catch(\Exception $e) {
            Alert::error('Error!', $e->getMessage());
            return redirect(url()->previous());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $waliKelas = WaliKelas::findOrFail($id);
        $kelas = Kelas::get();
        $guru = User::get();

        return view('pages.admin.wali-kelas.edit', compact('waliKelas', 'kelas', 'guru'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WaliKelasRequest $request, $id)
    {
        try {
            $waliKelas = WaliKelas::findOrFail($id);
            $waliKelas->user_id = $request->user_id;
            $waliKelas->kelas_id = $request->kelas_id;
            $waliKelas->save();

            Alert::success('Sukses!', 'Data berhasil diubah');
            return redirect(route('admin.wali-kelas.index'));
        } catch(\Exception $e) {
            Alert::error('Error!', $e->getMessage());
            return redirect(route('admin.wali-kelas.index'));

        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $waliKelas = WaliKelas::findOrFail($id);
        $waliKelas->delete();

        Alert::success('Sukses!', 'Data berhasil dihapus');
    }
}
