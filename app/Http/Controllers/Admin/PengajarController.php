<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Pengajar\PengajarCreateRequest, App\Http\Requests\Pengajar\PengajarUpdateRequest;
use App\Models\Mapel;
use App\Models\Kelas;
use App\Models\Pengajar;
use App\Models\TahunAjar;
use App\User;
use DataTables;
use Alert;

class PengajarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $pengajar = Pengajar::with('user', 'mapel', 'kelas', 'tahun_ajar')->get();
            return DataTables::of($pengajar)
                            ->addColumn('action', function($pengajar) {
                                $button = "<a href=".route('admin.pengajar.edit', $pengajar->id)." class='btn btn-success'><i class='far fa-edit'></i></a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<button class='btn btn-danger' data-remote='".route('admin.pengajar.destroy', $pengajar->id)."'><i class='fas fa-trash-alt'></i></button>";
                                return $button;
                            })
                            ->rawColumns(['materi','tanggal','action'])
                            ->make(true);
        }
        
        return view('pages.admin.pengajar.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mapel = Mapel::get();
        $kelas = Kelas::get();
        $guru = User::get();
        $tahunajar = TahunAjar::get();
        
        return view('pages.admin.pengajar.create', compact('kelas', 'guru', 'mapel', 'tahunajar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PengajarCreateRequest $request)
    {
        $pengajar = new Pengajar;
        $pengajar->user_id = $request->user_id;
        $pengajar->mapel_id = $request->mapel_id;
        $pengajar->kelas_id = $request->kelas_id;
        $pengajar->tahun_ajar_id = $request->tahun_ajar_id;
        $pengajar->save();

        Alert::success('Sukses!', 'Data berhasil ditambahkan');
        return redirect(route('admin.pengajar.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengajar = Pengajar::findOrFail($id);
        $mapel = Mapel::get();
        $kelas = Kelas::get();
        $guru = User::get();
        $tahunajar = TahunAjar::get();

        return view('pages.admin.pengajar.edit', compact('pengajar', 'mapel', 'kelas', 'guru', 'tahunajar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PengajarUpdateRequest $request, $id)
    {
        $pengajar = Pengajar::findOrFail($id);
        $pengajar->mapel_id = $request->mapel_id;
        $pengajar->kelas_id = $request->kelas_id;
        $pengajar->tahun_ajar_id = $request->tahun_ajar_id;
        $pengajar->save();

        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(route('admin.pengajar.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengajar = Pengajar::findOrFail($id);
        $pengajar->delete();

        Alert::success('Sukses!', 'Data berhasil dihapus');
        return redirect(route('admin.pengajar.index'));
    }
}
