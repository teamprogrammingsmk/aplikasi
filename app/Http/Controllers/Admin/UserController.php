<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\User\UserCreateRequest, App\Http\Requests\User\UserUpdateRequest;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role, Spatie\Permission\Models\Permission;
use \DB;
use DataTables;
use Alert;

class UserController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $user = User::get();
            return DataTables::of($user)
                            ->addColumn('action', function($user) {
                                $button = "<a href=".route('admin.guru.edit', $user->id)." class='btn btn-success'><i class='far fa-edit'></i></a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<button class='btn btn-danger' data-remote='".route('admin.guru.destroy', $user->id)."'><i class='fas fa-trash-alt'></i></button>";
                                return $button;
                            })
                            ->make(true);
        }

        return view('pages.admin.guru.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pangkat = User::getPossibleEnumValues('pangkat');
        $roles = Role::get();
        $permissions = Permission::get();

        return view('pages.admin.guru.create', compact('pangkat', 'roles', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $role = Role::findOrFail($request->role);
        $guru = User::create([
            'nip' => $request->nip,
            'name' => $request->name,
            'pangkat' => $request->pangkat,
            'username' => $request->username,
            'password' => Hash::make($request->password),
        ]);
        $guru->assignRole($role->name);
        $permissions = $request->post('permission');

        foreach($permissions as $p) {
            $permission = Permission::findOrFail($p);
            $guru->givePermissionTo($permission->name);
        }
        
        Alert::success('Sukses!', 'Data berhasil ditambahkan');
        return redirect(route('admin.guru.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guru = User::findOrFail($id);
        $roles = Role::get();
        $permissions = Permission::get();

        return view('pages.admin.guru.edit', compact('guru', 'roles', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $guru = User::findOrFail($id);
        $guru->name = $request->name;
        $guru->password = is_null($request->password) ? $guru->password : Hash::make($request->password);

        // Deleting Role in ModelHasRole
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        // New Model Role
        $guru->assignRole($request->role);
        // Deleting Permission in ModelHasPermission
        DB::table('model_has_permissions')->where('model_id',$id)->delete();
        // New Model Permission
        $guru->givePermissionTo($request->permission);
        $guru->save();

        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(route('admin.guru.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guru = User::findOrFail($id);
        $guru->delete();

        
        Alert::success('Sukses!', 'Data berhasil dihapus');
        return redirect(route('admin.guru.index'));
    }
}
