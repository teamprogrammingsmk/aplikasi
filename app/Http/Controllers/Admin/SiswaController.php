<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Http\Requests\Siswa\SiswaCreateRequest, App\Http\Requests\Siswa\SiswaUpdateRequest;
use DataTables;
use Alert;

class SiswaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $siswa = Siswa::with('kelas')->get();
            return DataTables::of($siswa)
                            ->addColumn('status', function($siswa){
                                $btnStatus = $siswa->status == 'aktif' ? 'success': 'danger';
                                return "<button class='btn text-capitalize btn-$btnStatus' disabled>$siswa->status</button>";
                            }) 
                            ->addColumn('action', function($siswa) {
                                $button = "<a href=".route('admin.siswa.edit', $siswa->id)." class='btn btn-success'><i class='far fa-edit'></i></a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<button class='btn btn-danger' data-remote='".route('admin.siswa.destroy', $siswa->id)."'><i class='fas fa-trash-alt'></i></button>";
                                return $button;
                            })
                            ->rawColumns(['status','action'])
                            ->make(true);
        }

        return view('pages.admin.siswa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = Kelas::get();

        return view('pages.admin.siswa.create', compact('kelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SiswaCreateRequest $request)
    {
        $siswa = new Siswa;
        $siswa->nis = $request->nis;
        $siswa->nisn = $request->nisn;
        $siswa->nama_siswa = $request->nama_siswa;
        $siswa->kelas_id = $request->kelas_id;
        $siswa->keterangan = $request->keterangan;
        $siswa->status = $request->status == 1 ? 'aktif' : 'nonaktif';
        $siswa->save();

        Alert::success('Sukses!', 'Data berhasil ditambahkan');
        return redirect(route('admin.siswa.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);
        $kelas = Kelas::get();

        return view('pages.admin.siswa.edit', compact('siswa', 'kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiswaUpdateRequest $request, $id)
    {
        $siswa = Siswa::findOrFail($id);
        if(auth()->user()->hasRole('super-admin')) {
            $siswa->nis = $request->nis;
            $siswa->nisn = $request->nisn;
        }
        $siswa->nama_siswa = $request->nama_siswa;
        $siswa->kelas_id = $request->kelas_id;
        $siswa->keterangan = $request->keterangan;
        $siswa->status = $request->status == 1 ? 'aktif' : 'nonaktif';
        $siswa->save();

        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(route('admin.siswa.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->delete();

        Alert::success('Sukses!', 'Data berhasil dihapus');
        return redirect(route('admin.siswa.index'));
    }
}
