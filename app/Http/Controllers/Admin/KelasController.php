<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Http\Requests\Kelas\KelasCreateRequest, App\Http\Requests\Kelas\KelasUpdateRequest;
use DataTables;
use Alert;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $kelas = Kelas::get();
            return DataTables::of($kelas)
                            ->addColumn('action', function($kelas) {
                                $button = "<a href=".route('admin.kelas.edit', $kelas->id)." class='btn btn-success'><i class='far fa-edit'></i></a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<button class='btn btn-danger' data-remote='".route('admin.kelas.destroy', $kelas->id)."'><i class='fas fa-trash-alt'></i></button>";
                                return $button;
                            })
                            ->rawColumns(['action'])
                            ->make(true);
        }


        return view('pages.admin.kelas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.kelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KelasCreateRequest $request)
    {
        $kelas = new Kelas;
        $kelas->nama_kelas = $request->nama_kelas;
        $kelas->save();
        
        Alert::success('Sukses!', 'Data berhasil ditambahkan');
        return redirect(route('admin.kelas.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = Kelas::findOrFail($id);

        return view('pages.admin.kelas.edit', compact('kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KelasUpdateRequest $request, $id)
    {
        $kelas = Kelas::findOrFail($id);
        $kelas->nama_kelas = $request->nama_kelas;
        $kelas->save();

        
        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(route('admin.kelas.index'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = Kelas::findOrFail($id);
        $kelas->delete();

        Alert::success('Sukses!', 'Data berhasil dihapus');
        return redirect(route('admin.kelas.index'));
    }
}
