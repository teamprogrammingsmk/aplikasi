<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\User\UserEditProfileRequest, App\Http\Requests\User\UserEditPasswordRequest;
use Illuminate\Support\Facades\Hash;
use Alert;

class UserController extends Controller
{
    public function index() {
        $user = User::findOrFail(auth()->user()->id);
    
        return view('pages.user.profile.index', compact('user'));
    }

    public function updateProfile(UserEditProfileRequest $request) {
        $user = User::findOrFail(auth()->user()->id);
        $user->name = $request->name;
        $user->save();

        Alert::success('Sukses!', 'Profile berhasil diubah');
        return redirect(route('user.index'));
    }

    public function password() {
        $user = User::findOrFail(auth()->user()->id);
    
        return view('pages.user.profile.password', compact('user'));
    }

    public function updatePassword(UserEditPasswordRequest $request) {
        $user = User::findOrFail(auth()->user()->id);
        $user->password = Hash::make($request->password);
        $user->save();

        Alert::success('Sukses!', 'Password berhasil diubah');
        return redirect(route('user.index'));
    }
}
