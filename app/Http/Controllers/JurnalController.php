<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Jurnal;
use App\Models\Pengajar;
use App\Models\Mapel;
use App\Models\Absen;
use App\Models\Siswa;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Jurnal\JurnalCreateRequest;
use DataTables;
use Alert;

class JurnalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $jurnal = Jurnal::with('kelas', 'mapel');
            if(!auth()->user()->hasRole('super-admin')) {
                $jurnal->where('user_id', auth()->user()->id);
            }
            return DataTables::of($jurnal->get())
                            ->addColumn('materi', function($jurnal) {
                                return \Illuminate\Support\Str::limit($jurnal->materi, 8);
                            })
                            ->addColumn('tanggal', function($jurnal) {
                                return \Carbon\Carbon::parse($jurnal->tanggal)->translatedFormat('l, d F Y');
                            }) 
                            ->addColumn('action', function($jurnal) {
                                $button = "<a href=".route('jurnal.show', $jurnal->id)." class='btn btn-info'><i class='far fa-eye'></i></a>";
                                $button .= "&nbsp;&nbsp;";
                                $button .= "<a href=".route('jurnal.edit', $jurnal->id)." class='btn btn-success'><i class='far fa-edit'></i></a>";
                                if(auth()->user()->hasRole('super-admin')) {
                                    $button .= "&nbsp;&nbsp;";
                                    $button .= "<button class='btn btn-danger' data-remote='".route('jurnal.destroy', $jurnal->id)."'><i class='fas fa-trash-alt'></i></button>";
                                }
                                return $button;
                            })
                            ->rawColumns(['materi','tanggal','action'])
                            ->make(true);
        }

        return view('pages.user.jurnal.index');
    }

    /**
     * Displaying for all user.
     */

    public function all(Request $request)
    {
        if($request->ajax()) {
            $jurnal = Jurnal::with('kelas', 'mapel')->get();
            return DataTables::of($jurnal)
                            ->addColumn('materi', function($jurnal) {
                                return \Illuminate\Support\Str::limit($jurnal->materi, 8);
                            })
                            ->addColumn('tanggal', function($jurnal) {
                                return \Carbon\Carbon::parse($jurnal->tanggal)->translatedFormat('l, d F Y');
                            }) 
                            ->addColumn('action', function($jurnal) {
                                $button = "<a href=".route('jurnal.show', $jurnal->id)." class='btn btn-info'><i class='far fa-eye'></i></a>";
                                return $button;
                            })
                            ->rawColumns(['materi','tanggal','action'])
                            ->make(true);
        }

        return view('pages.user.jurnal.all');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pengajar = Pengajar::where('user_id', auth()->user()->id)->groupBy('kelas_id')->get();
        $jurnal = Jurnal::jurnal_id();

        return view('pages.user.jurnal.create', compact('pengajar', 'jurnal'));
    }

    /**  
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JurnalCreateRequest $request)
    {
        $jurnalID = Jurnal::jurnal_id();
    
        try {
            $jurnal = new Jurnal;
            $jurnal->no_jurnal = $jurnalID;
            $jurnal->tanggal = $request->tanggal;
            $jurnal->user_id = auth()->user()->id;
            $jurnal->kelas_id = $request->kelas_id;
            $jurnal->mapel_id = $request->mapel_id;
            $jurnal->materi = $request->materi;
            $jurnal->save();

            foreach ($request->post('siswaid') as $key => $siswaid) {
                $file = $request->file('images_' . $siswaid);
                // Validate Image
                if($file) {
                    $request->validate([
                        'images_'.$siswaid => 'image|max:2048|mimes:jpeg,jpg,png'
                    ]);
                    
                    $ext = $file->guessExtension();
                    $filename = md5(time().$siswaid.$jurnalID).'.'.$ext;
                    $file->storeAs('images/absensi', $filename, 'public');
                }

                $data[$key]['siswaid'] = $siswaid;
                $data[$key]['presensi'] = $request->post('absen_' . $siswaid);
                $data[$key]['keterangan'] = $request->post('keterangan_' . $siswaid);
                $data[$key]['image'] = is_null($file) ? null : $filename;
            }
        
            foreach($data as $absensi) {
                $siswa = Siswa::where('id', $absensi['siswaid'])->first();

                $absen = new Absen;
                $absen->jurnal_id = $jurnal->id;
                $absen->siswa_id = $siswa->id;
                $absen->presensi = $absensi['presensi'];
                $absen->keterangan = $absensi['keterangan'];
                $absen->image = is_null($absensi['image']) ? null : $absensi['image'];
                $absen->save();
            }
            
        } catch (\Exception $e) {
            Alert::error('Error!', $e->getMessage());
            return redirect(route('jurnal.index'));
        }
        
        Alert::success('Sukses!', 'Data berhasil ditambahkan');
        return redirect(route('jurnal.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jurnal = Jurnal::findOrFail($id);
        $siswa = Siswa::where('kelas_id', $id)->orderBy('nama_siswa', 'asc')->get();
        $absen = Absen::where('jurnal_id', $id)->get();

        return view('pages.user.jurnal.see', compact('jurnal', 'siswa', 'absen'));
    }

    /**
     * Displaying detail absen by id.
     */

    public function show_detail($id) {
        $absen = Absen::where('id', $id)->with('jurnal')->first();

        return view('pages.user.jurnal.detail', compact('absen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jurnal = Jurnal::findOrFail($id);

        if ($jurnal->user_id != auth()->user()->id && !auth()->user()->hasRole('super-admin')) {
            return redirect(route('jurnal.index'))->with('danger', 'Error!');
        }

        $absen = Absen::where('jurnal_id', $id)->get();

        return view('pages.user.jurnal.edit', compact('jurnal', 'absen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $jurnal = Jurnal::findOrFail($id);
            $jurnal->tanggal = $request->tanggal;
            $jurnal->materi = $request->materi;
            $jurnal->save();
            
            foreach ($request->post('siswaid') as $key => $siswaid) {
                $file = $request->file('images_' . $siswaid);
                $absen = Absen::where('jurnal_id', $id)->where('siswa_id', $siswaid)->first();
                // Validate Image
                if($file) {
                    $request->validate([
                        'images_'.$siswaid => 'image|max:2048|mimes:jpeg,jpg,png'
                    ]);
                    if($absen->image) {
                        unlink(public_path('storage/images/absensi/' . $absen->image));
                    }
                    $ext = $file->guessExtension();
                    $filename = md5(time().$siswaid.$id).'.'.$ext;
                    $file->storeAs('images/absensi', $filename, 'public');
                }

                $data[$key]['siswaid'] = $siswaid;
                $data[$key]['presensi'] = $request->post('absen_' . $siswaid);
                $data[$key]['keterangan'] = $request->post('keterangan_' . $siswaid);
                $data[$key]['image'] = is_null($file) ? $absen->image : $filename;
            }
            
            foreach ($data as $absensi) {
                $siswa = Siswa::where('id', $absensi['siswaid'])->first();
                
                $absen = Absen::where('jurnal_id', $id)->where('siswa_id', $siswa->id)->first();
                $absen->presensi = $absensi['presensi'];
                $absen->keterangan = $absensi['keterangan'];
                $absen->image = $absensi['image'];
                $absen->save();
            }
        } catch(\Exception $e) {
            Alert::error('Error!', $e);
            return redirect(route('jurnal.index'));
        }

        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(route('jurnal.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jurnal = Jurnal::findOrFail($id);
        $absen = Absen::where('jurnal_id', $id)->get();
        
        // Deleting Image
        foreach($absen as $absensi) {
            if($absensi->image) {
                unlink(public_path('storage/images/absensi/' . $absensi->image));
            }
        }

        $jurnal->delete();
        
        Alert::success('Sukses!', 'Data berhasil dihapus');
        return redirect(route('jurnal.index'));
    }

    // FILTER
    public function siswa(Request $request)
    {
        $filter = $request->get('kelas_id');
        $students = Siswa::where('kelas_id', $filter)->orderBy('nama_siswa', 'asc')->get();

        $no = 1;
        foreach($students as $student) {
            $data[] = "<tr><td>$no</td><td><input type='hidden' name='siswaid[]' value=".$student->id."> $student->nama_siswa </td><td><input type='radio' name='absen_".$student->id."' value='hadir'></td><td><input type='radio' name='absen_".$student->id."' value='sakit'></td><td><input type='radio' name='absen_".$student->id."' value='ijin'></td><td><input type='radio' name='absen_".$student->id."' value='alpha' checked></td></td><td><textarea name='keterangan_".$student->id."' class='form-control mb-3 input-jurnal-keterangan' placeholder='Keterangan' rows='2'></textarea><input class='form-control input-jurnal-gambar' type='file' name='images_".$student->id."'></td></tr>";
        $no++;
        }

        return $data;
    }

    public function mapel(Request $request)
    {
        $pengajars = Pengajar::with('mapel')->where('user_id', auth()->user()->id)->where('kelas_id', $request->post('kelas_id'))->get();

        foreach($pengajars as $key => $pengajar) {
            $data[$key][] = "<option value='" . $pengajar->mapel->id . "'>" . $pengajar->mapel->nama_mapel . "</option>";
        }

        return $data;
    }
}
