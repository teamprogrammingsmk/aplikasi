<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Barryvdh\DomPDF\Facade as PDF;
use PDF;

class PrintController extends Controller
{
    public function print(){
        $pdf = PDF::loadView('pages/user/laporan/print/index');
        return $pdf->stream();
    }
}
