<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\WaliKelas;
use Alert;

class WaliKelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        $waliKelas = WaliKelas::with('kelas')->where('user_id', auth()->user()->id)->first();
        $kelas = Kelas::with('siswa')->where('id', $waliKelas->kelas_id)->get();

        return view('pages.user.wali-kelas.index', compact('waliKelas', 'kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);

        return view('pages.user.wali-kelas.edit', compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $siswa = Siswa::findOrFail($id);
            $file = $request->file('image');
            if($file) {
                $request->validate([
                    'image' => 'image|max:2048|mimes:jpeg,jpg,png'
                    ]);
                    
                if($siswa->image != 'default.jpg') {
                    unlink(public_path('storage/images/siswa/' . $siswa->image));
                }
                
                $ext = $file->guessExtension();
                $filename = md5(time().$id).'.'.$ext;
                $file->storeAs('images/siswa', $filename, 'public');
            }
                
            $siswa->keterangan = $request->keterangan;
            $siswa->image = is_null($file) ? $siswa->image : $filename;
            $siswa->save();
        } catch(\Exception $e) {
            Alert::error('Error!', $e);
            return redirect(route('wali-kelas.index'));
        }

        Alert::success('Sukses!', 'Data berhasil diubah');
        return redirect(route('wali-kelas.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
