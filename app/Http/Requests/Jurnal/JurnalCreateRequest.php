<?php

namespace App\Http\Requests\Jurnal;

use Illuminate\Foundation\Http\FormRequest;

class JurnalCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => 'required',
            'kelas_id' => 'required',
            'mapel_id' => 'required',
            'materi' => 'required',
        ];
    }
}
