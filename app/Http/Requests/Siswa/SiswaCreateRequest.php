<?php

namespace App\Http\Requests\Siswa;

use Illuminate\Foundation\Http\FormRequest;

class SiswaCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nis'           => 'required|unique:App\Models\Siswa,nis',
            'nisn'           => 'required|unique:App\Models\Siswa,nisn',
            'nama_siswa'    => 'required',
            'kelas_id'      => 'required'
        ];
    }
}
