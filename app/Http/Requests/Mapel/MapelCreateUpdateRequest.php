<?php

namespace App\Http\Requests\Mapel;

use Illuminate\Foundation\Http\FormRequest;

class MapelCreateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_mapel' => 'required',
            'kelompok' => 'required',
            'tingkat' => 'required',
            'keterangan' => 'required',
            'jumlah_jam' => 'required',
            'jurusan_id' => 'required',
        ];
    }
}
