<?php

namespace App\Http\Requests\Pengajar;

use Illuminate\Foundation\Http\FormRequest;

class PengajarCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'mapel_id' => 'required',
            'kelas_id' => 'required',
            'tahun_ajar_id' => 'required'
        ];
    }
}
