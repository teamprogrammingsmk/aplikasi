<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Jurusan;
use App\Models\Jurnal;
use App\Models\Pengajar;

class Mapel extends Model
{
    public $timestamps = false;
    protected $table = 'mapel';

    public function jurusan() {
        return $this->belongsTo(Jurusan::class);
    }

    public static function getPossibleEnumValues($name) {
        $instance = new static; // create an instance of the model to be able to get the table name
        $type = DB::select( DB::raw('SHOW COLUMNS FROM '.$instance->getTable().' WHERE Field = "'.$name.'"') )[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach(explode(',', $matches[1]) as $value){
            $v = trim( $value, "'" );
            $enum[] = $v;
        }
        return $enum;
    }

    public function jurnal() {
        return $this->hasMany(Jurnal::class);
    }

    public function pengajar() {
        return $this->hasMany(Pengajar::class);
    }
}
