<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\TahunAjar;
use App\User;

class Pengajar extends Model
{
    public $timestamps = false;
    protected $table = 'pengajar';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function mapel() {
        return $this->belongsTo(Mapel::class);
    }

    public function kelas() {
        return $this->belongsTo(Kelas::class);
    }
    
    public function tahun_ajar() {
        return $this->belongsTo(TahunAjar::class);
    }
}
