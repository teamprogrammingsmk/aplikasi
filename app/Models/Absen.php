<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Siswa;

class Absen extends Model
{
    public $timestamps = false;
    protected $table = 'absen';

    public function jurnal() {
        return $this->belongsTo(Jurnal::class);
    }

    public function siswa() {
        return $this->belongsTo(Siswa::class);
    }
}
