<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Kelas;

class WaliKelas extends Model
{
    public $timestamps = false;
    protected $table = 'wali_kelas';

    public static function hasUser($user_id) {
        return WaliKelas::where('user_id', $user_id)->first();
    }
    
    public static function hasKelas($kelas_id) {
        return WaliKelas::where('kelas_id', $kelas_id)->first();
    }

    public static function userHasWaliKelas($user_id) {
        return WaliKelas::where('user_id', $user_id)->first();
    } 

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function kelas() {
        return $this->belongsTo(Kelas::class);
    }
}
