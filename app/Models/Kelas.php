<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Siswa;
use App\Models\Jurnal;
use App\Models\Kelas;
use App\Models\WaliKelas;

class Kelas extends Model
{
    public $timestamps = false;
    protected $table = 'kelas';

    public function siswa() {
        return $this->hasMany(Siswa::class);
    }

    public function jurnal() {
        return $this->hasMany(Jurnal::class);
    }

    public function waliKelas() {
        return $this->hasOne(WaliKelas::class);
    }

    public function kelas() {
        return $this->belongsTo(Kelas::class);
    }
}
