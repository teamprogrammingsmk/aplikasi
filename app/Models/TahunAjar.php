<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pengajar;

class TahunAjar extends Model
{
    public $timestamps = false;
    protected $table = 'tahun_ajar';
    protected $guarded = ['id'];

    public function pengajar() {
        return $this->hasMany(Pengajar::class);
    }
}
