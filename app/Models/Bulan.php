<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Bulan extends Model
{
    public static function getBulan(){
        return  [
            [
                'no' => 1,
                'bulan' => 'Januari'
            ],
            [
                'no' => 2,
                'bulan' => 'Februari'
            ],
            [
                'no' => 3,
                'bulan' => 'Maret'
            ],
            [
                'no' => 4,
                'bulan' => 'April'
            ],
            [
                'no' => 5,
                'bulan' => 'Mei'
            ],
            [
                'no' => 6,
                'bulan' => 'Juni'
            ],
            [
                'no' => 7,
                'bulan' => 'Juli'
            ],
            [
                'no' => 8,
                'bulan' => 'Agustus'
            ],
            [
                'no' => 9,
                'bulan' => 'September'
            ],
            [
                'no' => 10,
                'bulan' => 'Oktober'
            ],
            [
                'no' => 11,
                'bulan' => 'November'
            ],
            [
                'no' => 12,
                'bulan' => 'Desember'
            ],
        ];
    }
}
