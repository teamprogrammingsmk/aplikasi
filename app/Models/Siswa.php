<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Absen;
use App\Models\Kelas;

class Siswa extends Model
{
    public $timestamps = false;
    protected $table = 'siswa';
    protected $guarded = [];

    public function absen() {
        return $this->hasMany(Absen::class, 'nisn', 'nisn');
    }

    public function kelas() {
        return $this->belongsTo(Kelas::class);
    }
}
