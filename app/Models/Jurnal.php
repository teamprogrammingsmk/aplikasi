<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Kelas;
use App\Models\Mapel;
use DB;

class Jurnal extends Model
{
    protected $table = 'jurnal';

    public static function jurnal_id() {
        $prefix = 'SMK';
        $date = date('ym');
        $delimit = '-';
        $query = Jurnal::orderBy('created_at', 'desc');

        if($query->count() > 0) {
            $jurnal = explode($delimit, $query->first()->no_jurnal);
            $newJurnal = ($jurnal[1] + 1);
            $value = $prefix.$date.$delimit.(sprintf('%04s', $newJurnal));
        } else {
            $value = $prefix.$date.$delimit.'0001';
        }

        return $value;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function absen() {
        return $this->hasMany(Absen::class);
    }

    public function kelas() {
        return $this->belongsTo(Kelas::class);
    }
    
    public function mapel() {
        return $this->belongsTo(Mapel::class);
    }
}
