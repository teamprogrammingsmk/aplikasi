<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;
    
    public static function setting()
    {
        foreach (Setting::all() as $setting) {
            $data[$setting['variable']] = $setting['value'];
        }

        return $data;
    }
}
