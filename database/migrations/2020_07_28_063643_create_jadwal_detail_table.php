<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_detail', function (Blueprint $table) {
            $table->id();
            $table->foreignId('jadwal_id');
            $table->enum('hari', ['Sen', 'Sel', 'Rab', 'Kam', 'Jum']);
            $table->foreignId('mapel_id');
            $table->string('sesi_awal');
            $table->string('sesi_akhir');
            
            $table->foreign('jadwal_id')->references('id')->on('jadwal')->onDelete('cascade');
            $table->foreign('mapel_id')->references('id')->on('mapel')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_detail');
    }
}
