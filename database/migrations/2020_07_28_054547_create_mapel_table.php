<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapel', function (Blueprint $table) {
            $table->id();
            $table->string('nama_mapel');
            $table->integer('jumlah_jam');
            $table->foreignId('jurusan_id');
            $table->enum('kelompok', ['A', 'B', 'C1', 'C2', 'C3', 'M']);
            $table->enum('tingkat', [10,11,12]);
            $table->text('keterangan')->nullable();
            $table->enum('status',['aktif','nonaktif']);
            $table->foreign('jurusan_id')->references('id')->on('jurusan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mapel');
    }
}
