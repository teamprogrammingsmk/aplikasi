<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Jurusan;

class Mapel extends Model
{
    public $timestamps = false;
    protected $table = 'mapel';

    public function jurusan() {
        return $this->belongsTo(Jurusan::class);
    }
}
