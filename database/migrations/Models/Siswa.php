<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Kelas;

class Siswa extends Model
{
    public $timestamps = false;
    protected $table = 'siswa';
    protected $primaryKey = 'nisn';

    public function kelas() {
        return $this->belongsTo(Kelas::class);
    }
}
