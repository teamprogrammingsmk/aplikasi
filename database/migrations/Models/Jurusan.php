<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Mapel;

class Jurusan extends Model
{
    public $timestamps = false;
    protected $table = 'jurusan';

    public function mapel() {
        return $this->hasMany(Mapel::class);
    }
}
