<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Siswa;

class Kelas extends Model
{
    public $timestamps = false;
    protected $table = 'kelas';

    public function siswa() {
        return $this->hasMany(Siswa::class);
    }
}
