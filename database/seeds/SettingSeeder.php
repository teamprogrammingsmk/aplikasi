<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([[
            'variable' => 'web_title',
            'type' => 'text',
            'value' => 'SMKN 1 Panji'
        ],[
            'variable' => 'web_alias',
            'type' => 'text',
            'value' => 'SISKASAJI'
        ],[
            'variable' => 'web_logo',
            'type' => 'file',
            'value' => 'smkn1panji.png'
        ]]);
    }
}
