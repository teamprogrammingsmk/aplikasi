<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->nip = '10000000';
        $user->name = 'Admin';
        $user->pangkat = 'Pengatur';
        $user->username = 'admin';
        $user->password = Hash::make('admin');
        $user->save();

        $user->assignRole(['super-admin']);
    }
}
