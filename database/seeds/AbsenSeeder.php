<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Arr;
class AbsenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=2233; $i < 2268 ; $i++) { 
            $name = ['hadir', 'sakit', 'ijin','alpha'];
            $random= Arr::random($name);
            DB::table('absen')->insert([
                // 'tanggal' => Carbon::now()->format('Y-m-d H:i:s'),
                'jurnal_id' => '65482100820',
                'siswa_id' => $i,
                'presensi' => $random
            ]);
            //X OTKP 2 //2268 //2233
        }
        
    }
}
