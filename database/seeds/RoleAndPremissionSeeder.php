<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleAndPremissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // PERMISSION
        $permissions = [
            'add-jurusan', 'edit-jurusan', 'delete-jurusan', 'see-jurusan',
            'add-jurnal', 'edit-jurnal', 'delete-jurnal', 'see-jurnal',
            'add-kelas', 'edit-kelas', 'delete-kelas', 'see-kelas',
            'add-siswa', 'edit-siswa', 'delete-siswa', 'see-siswa',
            'add-guru', 'edit-guru', 'delete-guru', 'see-guru',
            'add-mapel', 'edit-mapel', 'delete-mapel', 'see-mapel',
            'add-tahun-ajar', 'edit-tahun-ajar', 'delete-tahun-ajar', 'see-tahun-ajar',
            'add-pengajar', 'edit-pengajar', 'delete-pengajar', 'see-pengajar',
            'see-laporan', 'see-laporan-bk', 'see-laporan-sdm',
            'edit-settings'
        ];
        
        foreach($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }

        $roles = ['super-admin', 'admin', 'user'];

        foreach($roles as $role) {
            $insertRole = Role::create(['name' => $role]);

            if($role == 'super-admin') {
                $insertRole->givePermissionTo($permissions);
            }
        }
    }
}
